﻿using ElasticSearch.Models.Responses.Properties;
using ElasticSearchAPI.Models;
using ElasticSearchAPI.Models.ControllerHelpers;
using ElasticSearchAPI.Models.ReturnToClient;
using ElasticSearchCore.API.ElasticQueries;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ElasticSearchAPI.Controllers
{
    public class ElasticSearchPropertyController : ApiController
    {
        // GET: api/ElasticSearchProperty/5
        public ReturnToLegoLand Get(int id)
        {
            PropertyQuery propertyQuery = new PropertyQuery();
            PropertyHit[] hits = propertyQuery.GetPropertyByInventoryID((long)id).hits.hits;
            List<PropertyInfo> infos = new List<PropertyInfo>();
            List<RefinementDimensions> refinements = new List<RefinementDimensions>();
            Helpers helper = new Helpers();

            if (hits.Length >= 0)
            {
                helper.FillPropertyInfos(hits.ToList(), infos);
                refinements = helper.GetRefinements(hits.ToList());
            }

            ReturnToLegoLand returnToLegoLand = new ReturnToLegoLand()
            {
                featuredProperties = new List<PropertyInfo>(),
                propertyInfos = infos,
                currentDimensions = new List<RefinementDimensions>(),
                refinementDimensions = refinements,
                TotalRecords = infos.Count
            };

            return returnToLegoLand;
        }
    }
}

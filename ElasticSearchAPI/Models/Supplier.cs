﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class Supplier: ISupplier
    {
        public bool IsActive { get; set; }
        public long? Id { get; set; }
        public string GatewaySupplierId { get; set; }
        public GatewayProviderType GatewayProviderType { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneNumberToDisplayToAgent { get; }
        public string ReservationPhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public string URL { get; set; }
        public PhoneNumber AfterHoursPhone { get; set; }
        public string EscalationProcedure { get; set; }
        public string ContractAgreement { get; set; }
        public string RentalAgreement { get; set; }
        public string PropertyDisclaimer { get; set; }
        public bool IsActiveInGateway { get; set; }

        public PartialAddress Address { get; set; }
        public string Address1 { get; }
        public string Address2 { get; }
        public string City { get; }
        public string State { get; }
        public string PostalCode { get; }
        public string Country { get; }

        public PartialAddress BillingAddress { get; set; }
        public string BillingAddress1 { get; }
        public string BillingAddress2 { get; }
        public string BillingCity { get; }
        public string BillingState { get; }
        public string BillingCountry { get; }
        public string BillingPostalCode { get; }

        public long? PrimaryDestinationId { get; set; }
        public string PrimaryCountryCode { get; set; }
        public string PrimaryISOStateCode { get; set; }

        public int NumberOfUnits { get; set; }
        public string CCAccountNumber { get; set; }
        public string CCAccountTag { get; set; }
        public SoftwareType SoftwareType { get; set; }
        public string PromoCode { get; set; }
        public bool IsAvailableForBooking { get; set; }
        public bool SupplierProvidesRackRates { get; set; }
        public string LinkUrl { get; }
        public bool IsManualGateway { get; }
        public bool IsSecurityDepositProtectionRequired { get; set; }
        public int? Tier { get; set; }
        public bool DisplayGatewayNameWithConfirmationNumber { get; set; }
        public bool RetailerManagesInventory { get; set; }
        public BookWithSupplierType RetailerBooksWithSupplierType { get; set; }
        public bool AreExtranetNotificationsEnabled { get; }
        public ReadOnlyCollection<ExtranetNotification> ExtranetNotificationEmailAddresses { get; }
        public List<long> RateCalendarTemplateIds { get; }
        public List<long> FeeCalendarTemplateIds { get; }
        public long? NoteCollectionId { get; set; }
        public string ContactGroupIdentifier { get; set; }
        public bool HasCustomTaxesAndFees { get; set; }

        public void ActivateInGateway(IEventAggregator eventAggregator)
        {
            throw new NotImplementedException();
        }

        public void AddExtranetNotificationEmailAddresses(ExtranetNotification extranetNotificationEmailAddresses)
        {
            throw new NotImplementedException();
        }

        public void AddFeeCalendarTemplateId(long feeCalendarTemplateId)
        {
            throw new NotImplementedException();
        }

        public void ClearExtranetNotificationEmailAddresses()
        {
            throw new NotImplementedException();
        }

        public void DeactivateInGateway(IEventAggregator eventAggregator)
        {
            throw new NotImplementedException();
        }

        public void DisableExtranetNotifications(IEventAggregator eventAggregator)
        {
            throw new NotImplementedException();
        }

        public void EnableExtranetNotifications(IEventAggregator eventAggregator)
        {
            throw new NotImplementedException();
        }

        public void RemoveExtranetNotificationEmailAddresses(ExtranetNotification extranetNotificationEmailAddresses)
        {
            throw new NotImplementedException();
        }
    }
}
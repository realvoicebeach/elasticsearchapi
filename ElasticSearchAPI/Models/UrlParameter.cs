﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class UrlParameter
    {
        public UrlParameter(string name, IEnumerable<UrlParameterValue> values)
        {
            _name = name.ToLower();
            _values.AddRange(values);
        }

        private readonly string _name;
        public string Name
        {
            get
            {
                return _name;
            }
        }

        private readonly List<UrlParameterValue> _values = new List<UrlParameterValue>();
        public ReadOnlyCollection<UrlParameterValue> Values
        {
            get
            {
                return _values.AsReadOnly();
            }
        }

        public override string ToString()
        {
            var parts = new List<string>
            {
                Name
            };

            parts.AddRange(Values.Select(x => x.ToString()));

            return string.Join("_", parts.ToArray());
        }

        public static UrlParameter Parse(string parameterString)
        {
            string[] parts = parameterString.Split('_');

            string name = parts[0];

            var values = parts.Skip(1).Select(x => new UrlParameterValue(x));

            return new UrlParameter(name, values);
        }
    }
}
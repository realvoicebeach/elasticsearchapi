﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public sealed class CurrencyType : CodeValue<CurrencyType>, IComparable<CurrencyType>
    {
        public static class Codes
        {
            public const string US_DOLLAR = "USD";
            public const string CANADIAN_DOLLAR = "CAD";
        }

        public static readonly CurrencyType USDollar = new CurrencyType(Codes.US_DOLLAR, "USD");
        public static readonly CurrencyType CanadianDollar = new CurrencyType(Codes.CANADIAN_DOLLAR, "CAD");

        private CurrencyType(string code, string description) : base(code, description) { }

        /*
 * Currency
 *  Code        Country
    AED		United Arab Emirates
    AFA		Afghanistan
    ALL		Albania
    AMD		Armenia
    ANG		Netherlands Antilles
    AON		Angola
    ARS		Argentina
    AUD		Australia
    AUD		Christmas Island
    AUD		Cocos (Keeling) Islands
    AUD		Heard Island and McDonald Islands
    AUD		Kiribati
    AUD		Nauru
    AUD		Norfolk Island
    AUD		Tuvalu
    AWG		Aruba
    AZM		Azerbaijan
    BAM		Bosnia-Herzegovina
    BBD		Barbados
    BDT		Bangladesh
    BGL		Bulgaria
    BHD		Bahrain
    BIF		Burundi
    BMD		Bermuda
    BND		Brunei Darussalam
    BOB		Bolivia
    BRL		Brazil
    BSD		Bahamas
    BTN		Bhutan
    BWP		Botswana
    BYB		Belarus
    BZD		Belize
    CAD		Canada
    CDF		Congo, Dem. Republic
    CHF		Liechtenstein
    CHF		Switzerland
    CLP		Chile
    CNY		China
    COP		Colombia
    CRC		Costa Rica
    CUP		Cuba
    CVE		Cape Verde
    CYP		Cyprus
    CZK		Czech Rep.
    DJF		Djibouti
    DKK		Denmark
    DKK		Faroe Islands
    DKK		Greenland
    DOP		Dominican Republic
    DZD		Algeria
    ECS		Ecuador
    EEK		Estonia
    EGP		Egypt
    ERN		Eritrea
    ETB		Ethiopia
    EUR		Andorra
    EUR		Austria
    EUR		Belgium
    EUR		European Union
    EUR		Finland
    EUR		France
    EUR		French Guiana
    EUR		French Southern Territories
    EUR		Germany
    EUR		Greece
    EUR		Guadeloupe (French)
    EUR		Ireland
    EUR		Italy
    EUR		Luxembourg
    EUR		Martinique (French)
    EUR		Mayotte
    EUR		Monaco
    EUR		Montenegro
    EUR		Netherlands
    EUR		Portugal
    EUR		Reunion (French)
    EUR		Saint Pierre and Miquelon
    EUR		Spain
    EUR		Vatican
    FJD		Fiji
    FKP		Falkland Islands
    GBP		Great Britain
    GBP		Guernsey
    GBP		Isle of Man
    GBP		Jersey
    GBP		South Georgia & South Sandwich Islands
    GBP		U.K.
    GEL		Georgia
    GHC		Ghana
    GIP		Gibraltar
    GMD		Gambia
    GNF		Guinea
    GWP		Guinea Bissau
    GYD		Guyana
    HKD		Hong Kong
    HNL		Honduras
    HRK		Croatia
    HTG		Haiti
    HUF		Hungary
    IDR		Indonesia
    ILS		Israel
    INR		India
    IQD		Iraq
    IRR		Iran
    ISK		Iceland
    ITL		San Marino
    JMD		Jamaica
    JOD		Jordan
    JPY		Japan
    KES		Kenya
    KGS		Kyrgyzstan
    KHR		Cambodia
    KMF		Comoros
    KPW		Korea-North
    KRW		Korea-South
    KWD		Kuwait
    KYD		Cayman Islands
    KZT		Kazakhstan
    LAK		Laos
    LBP		Lebanon
    LKR		Sri Lanka
    LRD		Liberia
    LSL		Lesotho
    LTL		Lithuania
    LVL		Latvia
    LYD		Libya
    MAD		Morocco
    MAD		Western Sahara
    MDL		Moldova
    MGF		Madagascar
    MKD		Macedonia
    MMK		Myanmar
    MNT		Mongolia
    MOP		Macau
    MRO		Mauritania
    MTL		Malta
    MUR		Mauritius
    MVR		Maldives
    MWK		Malawi
    MXN		Mexico
    MYR		Malaysia
    MZM		Mozambique
    NAD		Namibia
    NGN		Nigeria
    NIC		Nicaragua
    NOK		Bouvet Island
    NOK		Norway
    NOK		Svalbard and Jan Mayen Islands
    NPR		Nepal
    NZD		Cook Islands
    NZD		New Zealand
    NZD		Niue
    NZD		Pitcairn Island
    NZD		Tokelau
    OMR		Oman
    PAB		Panama
    PEN		Peru
    PGK		Papua New Guinea
    PHP		Philippines
    PKR		Pakistan
    PLZ		Poland
    PYG		Paraguay
    QAR		Qatar
    QTQ		Guatemala
    ROL		Romania
    RSD		Serbia
    RUR		Russia
    RWF		Rwanda
    SAR		Saudi Arabia
    SBD		Solomon Islands
    SCR		Seychelles
    SDD		Sudan
    SEK		Sweden
    SGD		Singapore
    SHP		Saint Helena
    SIT		Slovenia
    SKK		Slovakia
    SLL		Sierra Leone
    SOD		Somalia
    SRG		Suriname
    STD		Sao Tome and Principe
    SVC		El Salvador
    SYP		Syria
    SZL		Swaziland
    THB		Thailand
    TJR		Tajikistan
    TMM		Turkmenistan
    TND		Tunisia
    TOP		Tonga
    TRL		Turkey
    TTD		Trinidad and Tobago
    TWD		Taiwan
    TZS		Tanzania
    UAG		Ukraine
    UGS		Uganda
    USD		American Samoa
    USD		British Indian Ocean Territory
    USD		Guam (USA)
    USD		Marshall Islands
    USD		Micronesia
    USD		Northern Mariana Islands
    USD		Palau
    USD		Puerto Rico
    USD		Turks and Caicos Islands
    USD		USA
    USD		USA Minor Outlying Islands
    USD		Virgin Islands (British)
    USD		Virgin Islands (USA)
    UYP		Uruguay
    UZS		Uzbekistan
    VND		Vietnam
    VUB		Venezuela
    VUV		Vanuatu
    WST		Samoa
    XAF		Cameroon
    XAF		Central African Republic
    XAF		Chad
    XAF		Congo
    XAF		Equatorial Guinea
    XAF		Gabon
    XCD		Anguilla
    XCD		Antarctica
    XCD		Antigua and Barbuda
    XCD		Dominica
    XCD		Grenada
    XCD		Montserrat
    XCD		Saint Kitts & Nevis Anguilla
    XCD		Saint Lucia
    XCD		Saint Vincent & Grenadines
    XOF		Benin
    XOF		Burkina Faso
    XOF		Ivory Coast
    XOF		Mali
    XOF		Niger
    XOF		Senegal
    XOF		Togo
    XPF		New Caledonia (French)
    XPF		Polynesia (French)
    XPF		Wallis and Futuna Islands
    YER		Yemen
    ZAR		South Africa
    ZMK		Zambia
    ZWD		Zimbabwe
 * 
 */
        public int CompareTo(CurrencyType other)
        {
            // Description sort if Code is equal ascending
            if (Code == other.Code)
            {
                return Description.CompareTo(other.Description);
            }
            // Default to sort by Code. [ascending]
            return Code.CompareTo(other.Code);
        }
    }
}
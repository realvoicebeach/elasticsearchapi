﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class GeoLocation : IValueObject, IEquatable<GeoLocation>
    {

        public GeoLocation(double latitude, double longitude)
        {
            if (latitude < -90 || latitude > 90)
            {
                throw new ArgumentOutOfRangeException("latitude", "Latitude must be between -90 and 90");
            }
            if (longitude < -180 || longitude > 180)
            {
                throw new ArgumentOutOfRangeException("longitude", "Longitude must be between -180 and 180");
            }

            _latitude = latitude;
            _longitude = longitude;
        }

        private readonly double _latitude;
        public double Latitude
        {
            get { return _latitude; }
        }

        private readonly double _longitude;
        public double Longitude
        {
            get { return _longitude; }
        }

        private const double LocationEpsilon = 0.000000001;
        public int CompareTo(GeoLocation other)
        {
            if (other == null)
            {
                return 1;
            }
            if (Math.Abs(other.Latitude - Latitude) < LocationEpsilon)
            {
                return Latitude.CompareTo(other.Latitude);
            }
            return Longitude.CompareTo(other.Longitude);
        }

        public bool Equals(GeoLocation other)
        {
            return other != null && Math.Abs(Latitude - other.Latitude) < LocationEpsilon && Math.Abs(Longitude - other.Longitude) < LocationEpsilon;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return Equals((GeoLocation)obj);
        }

        public override int GetHashCode()
        {
            int h1 = Latitude.GetHashCode();
            int h2 = Longitude.GetHashCode();
            return ObjectUtilities.CombineHashCodes(h1, h2);
        }

        public static bool operator ==(GeoLocation lhs, GeoLocation rhs)
        {
            if (ReferenceEquals(lhs, null) && ReferenceEquals(rhs, null))
            {
                return true;
            }
            if (ReferenceEquals(lhs, null))
            {
                return false;
            }
            if (ReferenceEquals(rhs, null))
            {
                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(GeoLocation lhs, GeoLocation rhs)
        {
            return !(lhs == rhs);
        }

        public static GeoLocation Parse(string latitude, string longitude)
        {
            return Parse(latitude.ToDoubleNullWhenEmpty(), longitude.ToDoubleNullWhenEmpty());
        }

        public static GeoLocation Parse(double? latitude, double? longitude)
        {
            if (!latitude.HasValue || !longitude.HasValue || !IsValid(latitude.Value, longitude.Value))
            {
                return null;
            }
            return new GeoLocation(latitude.Value, longitude.Value);
        }

        public static bool IsValid(double latitude, double longitude)
        {
            if (latitude < -90 || latitude > 90)
            {
                return false;
            }
            if (longitude < -180 || longitude > 180)
            {
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            return string.Format("{{{0}, {1}}}", Latitude, Longitude);
        }
    }
}
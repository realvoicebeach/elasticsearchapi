﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchAPI.Models.Interfaces
{
    public interface IBasicRentalUnit
    {
        long Id { get; }
        long InventoryId { get; }
    }
}

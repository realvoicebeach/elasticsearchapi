﻿namespace ElasticSearchAPI.Models.Interfaces
{
    public interface IPersistedRentalUnitWithRoomType
    {
        long RentalUnitId { get; }
        IRoomType RoomType { set; }
    }
}
﻿using System.Collections.ObjectModel;

namespace ElasticSearchAPI.Models.Interfaces
{
    public interface IFloorPlan : IValueObject
    {
        FloorPlanType FloorPlanType { get; }
        BedroomType Bedrooms { get; }
        ReadOnlyCollection<string> BonusRooms { get; }
        string FloorPlanDescription { get; }
    }
}
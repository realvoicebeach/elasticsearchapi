﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchAPI.Models.Interfaces
{
    public interface IRoomType
    {
        string RoomName { get; set; }
        string RoomClassPrefix { get; set; }
        string RoomClassSuffix { get; set; }
        IFloorPlan FloorPlan { get; set; }
        string RoomTypeName { get; }
        ReadOnlyCollection<string> Views { get; }
        string ViewsDisplay { get; }
        ReadOnlyCollection<string> SplurgeAmenities { get; }
        string SplurgeAmenitiesDisplay { get; }
        string SplurgeAmenitiesDisplayWithPlusSeparator { get; }
        string SplurgeView { get; set; }
        ReadOnlyCollection<string> ViewsIncludingSplurgeView { get; }
        void AddView(string roomTypeView);
        void RemoveView(string roomTypeView);
        void AddSplurgeAmenity(string splurgeAmenity);
        void RemoveSplurgeAmenity(string splurgeAmenity);
        void RemoveAllViews();
        void RemoveAllSplurgeAmenities();
    }
}

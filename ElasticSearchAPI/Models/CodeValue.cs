﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

namespace ElasticSearchAPI.Models
{
    [Serializable]
    public abstract class CodeValue
    {
        private readonly string _code;
        private readonly string _description;

        protected CodeValue(string code, string description)
        {
            _code = code;
            _description = description;
        }

        public string Code
        {
            get { return _code; }
        }

        public string Description
        {
            get { return _description; }
        }

        public override string ToString()
        {
            return Description;
        }

        public bool IsBlank
        {
            get { return string.IsNullOrEmpty(_code); }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            var codeValue = (CodeValue)obj;
            return Equals(_code, codeValue._code);
        }

        public override int GetHashCode()
        {
            return _code.GetHashCode();
        }

        public static bool operator ==(CodeValue lhs, CodeValue rhs)
        {
            if (ReferenceEquals(lhs, null) && ReferenceEquals(rhs, null))
            {
                return true;
            }
            if (ReferenceEquals(lhs, null))
            {
                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(CodeValue lhs, CodeValue rhs)
        {
            return !(lhs == rhs);
        }
    }

    [Serializable]
    public abstract class CodeValue<T> : CodeValue where T : CodeValue<T>
    {
        private static readonly CodeValueMap<T> _map;

        static CodeValue()
        {
            _map = new CodeValueMap<T>();

            FieldInfo[] fields = typeof(T).GetFields(BindingFlags.Static | BindingFlags.Public);
            foreach (FieldInfo info in fields)
            {
                info.GetValue(null); // doing this to force the static code values to load
            }
        }

        protected CodeValue(string code, string description) : this(code, description, true)
        {
        }

        protected CodeValue(string code, string description, bool addToMap) : base(code, description)
        {
            if (addToMap)
            {
                _map.Add((T)this);
            }
        }

        public static CodeValueMap<T> Map
        {
            get { return _map; }
        }

        public static ReadOnlyCollection<T> OrderedByDescriptionList
        {
            get { return _map.List.OrderBy(x => x.Description).ToList().AsReadOnly(); }
        }

        public static T Lookup(string code)
        {
            return _map.Lookup(code);
        }
    }

    [Serializable]
    public class GenericCodeValue : CodeValue<GenericCodeValue>
    {
        public GenericCodeValue(string code, string description)
            : base(code, description)
        {
        }
    }
}
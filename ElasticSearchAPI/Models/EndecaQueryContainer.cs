﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class SelectedDimension
    {
        public string name { get; set; }
        public string searchUrl { get; set; }
    }
    public class SortKey2
    {
        public string Key { get; set; }
        public bool IsDescending { get; set; }
    }
    public class EndecaQueryContainer
    {
        public string BaseQuery { get; set; }
        public SelectedDimension[] Dimensions { get; set; }
        public string SearchPhrase { get; set; }
        public int MaxReturns { get; set; }
        public int RecordOffset { get; set; }
        public SortKey2 SortKey { get; set; }
    }
}
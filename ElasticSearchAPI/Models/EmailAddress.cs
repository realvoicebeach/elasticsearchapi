﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class EmailAddress : IValueObject
    {
        private readonly string _emailAddress;
        public static readonly string RegexExpression = "\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

        public EmailAddress(string emailAddress)
        {
            if (!IsValid(emailAddress))
            {
                throw new ArgumentException("The specified value must be a valid email address.", "emailAddress");
            }

            _emailAddress = emailAddress.Trim();
        }

        public string Value
        {
            get
            {
                return _emailAddress;
            }
        }

        public string UserName
        {
            get { return Value.Substring(0, Value.IndexOf('@')); }
        }

        public override string ToString()
        {
            return Value;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var address = obj as EmailAddress;

            if (address != null)
            {
                return this == address;
            }

            return false;
        }

        public static bool operator ==(EmailAddress left, EmailAddress right)
        {
            // if it's the same reference (including both null), they're equal
            if (ReferenceEquals(left, right)) return true;

            // otherwise, if either is null then they're not equal 
            if (ReferenceEquals(null, left)) return false;
            if (ReferenceEquals(null, right)) return false;

            return (string.Compare(left.Value, right.Value, true) == 0);
        }

        public static bool operator !=(EmailAddress left, EmailAddress right)
        {
            return !(left == right);
        }

        public static bool IsValid(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress) || emailAddress.Trim().Equals(string.Empty))
            {
                return false;
            }

            var r = new Regex(RegexExpression);
            return r.IsMatch(emailAddress.Trim());
        }

        public override int GetHashCode()
        {
            return (_emailAddress != null ? _emailAddress.GetHashCode() : 0);
        }

        public static EmailAddress Parse(string emailAddress)
        {
            return IsValid(emailAddress) ? new EmailAddress(emailAddress) : null;
        }
    }
}
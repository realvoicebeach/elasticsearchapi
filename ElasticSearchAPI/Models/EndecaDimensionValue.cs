﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class EndecaDimensionValue
    {
        private readonly EndecaDimension _dimension;
        private readonly string _value;

        public EndecaDimensionValue(EndecaDimension dimension, string value)
        {
            _dimension = dimension;
            _value = value;
        }

        public EndecaDimension Dimension
        {
            get { return _dimension; }
        }

        public string Value
        {
            get { return _value; }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var other = (EndecaDimensionValue)obj;

            return Dimension == other.Dimension && Value == other.Value;
        }

        public override int GetHashCode()
        {
            return ObjectUtilities.CombineHashCodes(Dimension.GetHashCode(), Value.GetHashCode());
        }

        public static bool operator ==(EndecaDimensionValue lhs, EndecaDimensionValue rhs)
        {
            if (ReferenceEquals(lhs, null) && ReferenceEquals(rhs, null))
            {
                return true;
            }
            if (ReferenceEquals(lhs, null))
            {
                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(EndecaDimensionValue lhs, EndecaDimensionValue rhs)
        {
            return !(lhs == rhs);
        }

        public override string ToString()
        {
            return string.Format("{0}={1}", Dimension.Description, Value);
        }

    }
}
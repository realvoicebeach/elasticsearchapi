﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class RentalUnitType : CodeValue<RentalUnitType>
    {
        public static class Codes
        {
            public const string COMPLEX = "COMPLEX";
            public const string STANDALONE = "STANDALONE";
        }

        public readonly static RentalUnitType Complex = new RentalUnitType(Codes.COMPLEX, "Complex");
        public readonly static RentalUnitType StandAlone = new RentalUnitType(Codes.STANDALONE, "Stand-Alone");

        public RentalUnitType(string code, string description) : base(code, description)
        {
        }
    }
}
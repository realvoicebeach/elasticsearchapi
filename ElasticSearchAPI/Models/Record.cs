﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class Record
    {
        public int Spec { get; set; }
        public PropertyInfo PropertyInfo { get; set; }
        //public IRentalUnitContent RentalUnitContent { get; set; }
        public Dictionary<string, List<string>> DimensionNames { get; set; }

        public string DestinationType
        {
            get
            {
                string destinationType = "default";
                if (DimensionNames != null && DimensionNames.ContainsKey("DESTINATIONTYPECATEGORY") &&
                    DimensionNames["DESTINATIONTYPECATEGORY"].Count > 0)
                {
                    List<string> types = DimensionNames["DESTINATIONTYPECATEGORY"];
                    if (types.Any(dt => dt.ToUpper().Equals("SKI")))
                    {
                        destinationType = "Ski";
                    }
                    else
                    {
                        destinationType = types[0];
                    }
                }
                return destinationType;
            }
        }

        public string Destination
        {
            get
            {
                if (DimensionNames != null && DimensionNames.ContainsKey("DESTINATION") && DimensionNames["DESTINATION"].Count > 0)
                {
                    return DimensionNames["DESTINATION"][0];
                }

                return null;
            }
        }

        public List<DimensionRefinement> DimensionRefinements { get; set; }
    }
}
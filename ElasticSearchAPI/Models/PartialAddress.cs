﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class PartialAddress : IValueObject
    {
        private const char Delimiter = '~';

        public static PartialAddress FromDelimitedString(string delimitedString)
        {
            if (delimitedString == null)
            {
                throw new ArgumentNullException("delimitedString");
            }

            string[] addressPieces = delimitedString.Split(Delimiter);

            if (addressPieces.Length != 6)
            {
                return new PartialAddress(null, null, null, null, null, "US");
            }

            string countryCode = addressPieces[5];
            if (string.IsNullOrEmpty(countryCode) || string.Compare(countryCode, "USA", true) == 0)
            {
                countryCode = "US";
            }
            else
            {
                countryCode = countryCode.ToUpper();
            }

            return new PartialAddress(addressPieces[0].NullWhenEmpty(),
                addressPieces[1].NullWhenEmpty(),
                addressPieces[2].NullWhenEmpty(),
                addressPieces[3].NullWhenEmpty(),
                addressPieces[4].NullWhenEmpty(),
                countryCode);
        }

        public PartialAddress(string address1, string address2, string city, string stateCode, string postalCode, string countryCode)
        {
            _address1 = address1.NullWhenEmpty();
            _address2 = address2.NullWhenEmpty();
            _city = city.NullWhenEmpty();
            _stateCode = stateCode.NullWhenEmpty();
            _postalCode = postalCode.NullWhenEmpty();
            _countryCode = countryCode.NullWhenEmpty();
        }

        private readonly string _address1;
        public string Address1
        {
            get
            {
                return _address1;
            }
        }

        private readonly string _address2;
        public string Address2
        {
            get
            {
                return _address2;
            }
        }

        private string _city;
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
            }
        }

        private string _stateCode;
        public string StateCode
        {
            get
            {
                return _stateCode;
            }
            set
            {
                _stateCode = value;
            }
        }

        private readonly string _postalCode;
        public string PostalCode
        {
            get
            {
                return _postalCode;
            }
        }

        private string _countryCode;
        public string CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                _countryCode = value;
            }
        }

        public bool IsComplete
        {
            get
            {
                return !string.IsNullOrEmpty(Address1) &&
                    !string.IsNullOrEmpty(City) &&
                    !string.IsNullOrEmpty(StateCode) &&
                    !string.IsNullOrEmpty(PostalCode) &&
                    !string.IsNullOrEmpty(CountryCode);
            }
        }

        public bool HasValueForAnyField
        {
            get
            {
                return !string.IsNullOrEmpty(Address1) ||
                    !string.IsNullOrEmpty(Address2) ||
                    !string.IsNullOrEmpty(City) ||
                    !string.IsNullOrEmpty(StateCode) ||
                    !string.IsNullOrEmpty(PostalCode) ||
                    !string.IsNullOrEmpty(CountryCode);
            }
        }

        public override string ToString()
        {
            return string.Format("{0}{6}{1}{6}{2}{6}{3}{6}{4}{6}{5}", Address1, Address2, City, StateCode, PostalCode, CountryCode, Delimiter);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var other = (PartialAddress)obj;

            return Address1 == other.Address1 &&
                   Address2 == other.Address2 &&
                   City == other.City &&
                   StateCode == other.StateCode &&
                   PostalCode == other.PostalCode &&
                   CountryCode == other.CountryCode;
        }

        public override int GetHashCode()
        {
            return ObjectUtilities.CombineHashCodes(CountryCode == null ? 0 : CountryCode.GetHashCode(),
                ObjectUtilities.CombineHashCodes(PostalCode == null ? 0 : PostalCode.GetHashCode(),
                ObjectUtilities.CombineHashCodes(StateCode == null ? 0 : StateCode.GetHashCode(),
                ObjectUtilities.CombineHashCodes(City == null ? 0 : City.GetHashCode(),
                ObjectUtilities.CombineHashCodes(Address1 == null ? 0 : Address1.GetHashCode(),
                Address2 == null ? 0 : Address2.GetHashCode())))));
        }

        public static bool operator ==(PartialAddress lhs, PartialAddress rhs)
        {
            if (ReferenceEquals(lhs, null) && ReferenceEquals(rhs, null))
            {
                return true;
            }
            if (ReferenceEquals(lhs, null))
            {
                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(PartialAddress lhs, PartialAddress rhs)
        {
            return !(lhs == rhs);
        }

        public string FullAddress
        {
            get
            {
                string city = string.Empty;
                string state = string.Empty;
                string country = string.Empty;
                if (!String.IsNullOrEmpty(City))
                {
                    city = City;
                    if (!String.IsNullOrEmpty(StateCode))
                    {
                        state = StateCode;
                        if (!String.IsNullOrEmpty(CountryCode))
                        {
                            country = CountryCode;
                        }
                    }
                }
                return IsComplete ? string.Format("{0},{1},{2}", city, state, country) : String.Empty;
            }
        }
    }
}
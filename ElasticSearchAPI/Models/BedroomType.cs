﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class BedroomType : CodeValue<BedroomType>
    {
        private static readonly Dictionary<string, BedroomType> _mapBydescription = new Dictionary<string, BedroomType>();

        public static class Codes
        {
            public const string HOTEL_ROOM = "Hotel Room";
            public const string LOFT_HOTEL_ROOM = "Loft Hotel Room";
            public const string SUITE = "Suite";
            public const string LOFT_SUITE = "Loft Suite";
            public const string STUDIO = "Studio";
            public const string DORM_ROOM = "Dorm Room";
            public const string ONE_BED_ROOM = "1BR";
            public const string TWO_BED_ROOMS = "2BR";
            public const string THREE_BED_ROOMS = "3BR";
            public const string FOUR_BED_ROOMS = "4BR";
            public const string FIVE_BED_ROOMS = "5BR";
            public const string SIX_BED_ROOMS = "6BR";
            public const string SEVEN_BED_ROOMS = "7BR";
            public const string EIGHT_BED_ROOMS = "8BR";
            public const string NINE_BED_ROOMS = "9BR";
            public const string TEN_BED_ROOMS = "10BR";
            public const string ELEVEN_BED_ROOMS = "11BR";
            public const string TWELVE_BED_ROOMS = "12BR";
            public const string THIRTEEN_BED_ROOMS = "13BR";
            public const string FOURTEEN_BED_ROOMS = "14BR";
            public const string FIFTEEN_BED_ROOMS = "15BR";
            public const string SIXTEEN_BED_ROOMS = "16BR";
            public const string SEVENTEEN_BED_ROOMS = "17BR";
            public const string EIGHTEEN_BED_ROOMS = "18BR";
            public const string NINETEEN_BED_ROOMS = "19BR";
            public const string TWENTY_BED_ROOMS = "20BR";
            public const string TWENTY_ONE_BED_ROOM = "21BR";
            public const string TWENTY_TWO_BED_ROOMS = "22BR";
            public const string TWENTY_THREE_BED_ROOMS = "23BR";
            public const string TWENTY_FOUR_BED_ROOMS = "24BR";
            public const string TWENTY_FIVE_BED_ROOMS = "25BR";
            public const string TWENTY_SIX_BED_ROOMS = "26BR";
            public const string TWENTY_SEVEN_BED_ROOMS = "27BR";
            public const string TWENTY_EIGHT_BED_ROOMS = "28BR";
            public const string TWENTY_NINE_BED_ROOMS = "29BR";
            public const string THIRTY_BED_ROOMS = "30BR";
        }

        public readonly static BedroomType HotelRoom = new BedroomType(Codes.HOTEL_ROOM, "Hotel Room", 0);
        public readonly static BedroomType LoftHotelRoom = new BedroomType(Codes.LOFT_HOTEL_ROOM, "Loft Hotel Room", 0);
        public readonly static BedroomType Suite = new BedroomType(Codes.SUITE, "Suite", 0);
        public readonly static BedroomType LoftSuite = new BedroomType(Codes.LOFT_SUITE, "Loft Suite", 0);
        public readonly static BedroomType Studio = new BedroomType(Codes.STUDIO, "Studio", 0);
        public readonly static BedroomType DormRoom = new BedroomType(Codes.DORM_ROOM, "Dorm Room", 0);
        public readonly static BedroomType OneBedRoom = new BedroomType(Codes.ONE_BED_ROOM, "1BR", 1);
        public readonly static BedroomType TwoBedRooms = new BedroomType(Codes.TWO_BED_ROOMS, "2BR", 2);
        public readonly static BedroomType ThreeBedRooms = new BedroomType(Codes.THREE_BED_ROOMS, "3BR", 3);
        public readonly static BedroomType FourBedRooms = new BedroomType(Codes.FOUR_BED_ROOMS, "4BR", 4);
        public readonly static BedroomType FiveBedRooms = new BedroomType(Codes.FIVE_BED_ROOMS, "5BR", 5);
        public readonly static BedroomType SixBedRooms = new BedroomType(Codes.SIX_BED_ROOMS, "6BR", 6);
        public readonly static BedroomType SevenBedRooms = new BedroomType(Codes.SEVEN_BED_ROOMS, "7BR", 7);
        public readonly static BedroomType EightBedRooms = new BedroomType(Codes.EIGHT_BED_ROOMS, "8BR", 8);
        public readonly static BedroomType NineBedRooms = new BedroomType(Codes.NINE_BED_ROOMS, "9BR", 9);
        public readonly static BedroomType TenBedRooms = new BedroomType(Codes.TEN_BED_ROOMS, "10BR", 10);
        public readonly static BedroomType ElevenBedRooms = new BedroomType(Codes.ELEVEN_BED_ROOMS, "11BR", 11);
        public readonly static BedroomType TwelveBedRooms = new BedroomType(Codes.TWELVE_BED_ROOMS, "12BR", 12);
        public readonly static BedroomType TherteenBedRooms = new BedroomType(Codes.THIRTEEN_BED_ROOMS, "13BR", 13);
        public readonly static BedroomType FourteenBedRooms = new BedroomType(Codes.FOURTEEN_BED_ROOMS, "14BR", 14);
        public readonly static BedroomType FifteenBedRooms = new BedroomType(Codes.FIFTEEN_BED_ROOMS, "15BR", 15);
        public readonly static BedroomType SixteenBedRooms = new BedroomType(Codes.SIXTEEN_BED_ROOMS, "16BR", 16);
        public readonly static BedroomType SeventeenBedRooms = new BedroomType(Codes.SEVENTEEN_BED_ROOMS, "17BR", 17);
        public readonly static BedroomType EighteenBedRooms = new BedroomType(Codes.EIGHTEEN_BED_ROOMS, "18BR", 18);
        public readonly static BedroomType NineteenBedRooms = new BedroomType(Codes.NINETEEN_BED_ROOMS, "19BR", 19);
        public readonly static BedroomType TwentyBedRooms = new BedroomType(Codes.TWENTY_BED_ROOMS, "20BR", 20);
        public readonly static BedroomType TwentyOneBedRoom = new BedroomType(Codes.TWENTY_ONE_BED_ROOM, "21BR", 21);
        public readonly static BedroomType TwentyTwoBedRooms = new BedroomType(Codes.TWENTY_TWO_BED_ROOMS, "22BR", 22);
        public readonly static BedroomType TwentyThreeBedRooms = new BedroomType(Codes.TWENTY_THREE_BED_ROOMS, "23BR", 23);
        public readonly static BedroomType TwentyFourBedRooms = new BedroomType(Codes.TWENTY_FOUR_BED_ROOMS, "24BR", 24);
        public readonly static BedroomType TwentyFiveBedRooms = new BedroomType(Codes.TWENTY_FIVE_BED_ROOMS, "25BR", 25);
        public readonly static BedroomType TwentySixBedRooms = new BedroomType(Codes.TWENTY_SIX_BED_ROOMS, "26BR", 26);
        public readonly static BedroomType TwentySevenBedRooms = new BedroomType(Codes.TWENTY_SEVEN_BED_ROOMS, "27BR", 27);
        public readonly static BedroomType TwentyEightBedRooms = new BedroomType(Codes.TWENTY_EIGHT_BED_ROOMS, "28BR", 28);
        public readonly static BedroomType TwentyNineBedRooms = new BedroomType(Codes.TWENTY_NINE_BED_ROOMS, "29BR", 29);
        public readonly static BedroomType ThirtyBedRooms = new BedroomType(Codes.THIRTY_BED_ROOMS, "30BR", 30);

        public BedroomType(string code, string description, int bedroomCount) : base(code, description)
        {
            _bedroomCount = bedroomCount;
            _mapBydescription.Add(description, this);
        }

        private readonly int _bedroomCount;
        public int BedroomCount
        {
            get { return _bedroomCount; }
        }
    }
}
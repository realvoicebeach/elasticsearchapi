﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class BuildingType : CodeValue<BuildingType>
    {
        public static class Codes
        {
            public const string HOTEL_HOTEL = "HOTEL_HOTEL";
            public const string HOTEL_ALLINCLUSIVE = "HOTEL_ALLINCLUSIVE";
            public const string HOTEL_INN = "HOTEL_INN";
            public const string HOTEL_LODGE = "HOTEL_LODGE";
            public const string BEDBREAKFAST_BEDBREAKFAST = "BEDBREAKFAST_BEDBREAKFAST";
            public const string BEDBREAKFAST_COTTAGE = "BEDBREAKFAST_COTTAGE";
            public const string BEDBREAKFAST_INN = "BEDBREAKFAST_INN";
            public const string BEDBREAKFAST_LODGE = "BEDBREAKFAST_LODGE";
            public const string CONDO_CONDO = "CONDO_CONDO";
            public const string CONDO_TOWNHOME = "CONDO_TOWNHOME";
            public const string APARTMENT_APARTMENT = "APARTMENT_APARTMENT";
            public const string HOME_VACATIONHOME = "HOME_VACATIONHOME";
            public const string HOME_CABINLODGECOTTAGE = "HOME_CABINLODGECOTTAGE";
            public const string HOME_CHALET = "HOME_CHALET";
            public const string HOME_VILLA = "HOME_VILLA";
            public const string HOME_BUNGALOW = "HOME_BUNGALOW";
            public const string HOME_FARM = "HOME_FARM";
            public const string HOME_GUESTHOUSE = "HOME_GUESTHOUSE";
            public const string CAMPINGGROUND_CAMPINGGROUND = "CAMPINGGROUND_CAMPINGGROUND";
            public const string YACHT_YACHT = "YACHT_YACHT";
        }

        public readonly static BuildingType HotelHotel = new BuildingType(Codes.HOTEL_HOTEL, "Hotel", BuildingCategory.Hotel);
        public readonly static BuildingType HotelAllInclusive = new BuildingType(Codes.HOTEL_ALLINCLUSIVE, "All Inclusive Hotel", BuildingCategory.Hotel);
        public readonly static BuildingType HotelInn = new BuildingType(Codes.HOTEL_INN, "Hotel Inn", BuildingCategory.Hotel);
        public readonly static BuildingType HotelLodge = new BuildingType(Codes.HOTEL_LODGE, "Hotel Lodge", BuildingCategory.Hotel);
        public readonly static BuildingType BedBreakfastBedBreakFast = new BuildingType(Codes.BEDBREAKFAST_BEDBREAKFAST, "Bed & Breakfast", BuildingCategory.BedBreakfast);
        public readonly static BuildingType BedBreakfastCottage = new BuildingType(Codes.BEDBREAKFAST_COTTAGE, "Cottage", BuildingCategory.BedBreakfast);
        public readonly static BuildingType BedBreakfastInn = new BuildingType(Codes.BEDBREAKFAST_INN, "Bed & Breakfast Inn", BuildingCategory.BedBreakfast);
        public readonly static BuildingType BedBreakfastLodge = new BuildingType(Codes.BEDBREAKFAST_LODGE, "Bed & Breakfast Lodge", BuildingCategory.BedBreakfast);
        public readonly static BuildingType CondoCondo = new BuildingType(Codes.CONDO_CONDO, "Condo", BuildingCategory.Condo);
        public readonly static BuildingType CondoTownhome = new BuildingType(Codes.CONDO_TOWNHOME, "Townhome", BuildingCategory.Condo);
        public readonly static BuildingType ApartmentApartment = new BuildingType(Codes.APARTMENT_APARTMENT, "Apartment", BuildingCategory.Apartment);
        public readonly static BuildingType HomeVacationHome = new BuildingType(Codes.HOME_VACATIONHOME, "Vacation Home", BuildingCategory.Home);
        public readonly static BuildingType HomeCabinLodgeCottage = new BuildingType(Codes.HOME_CABINLODGECOTTAGE, "Cabin / Lodge / Cottage", BuildingCategory.Home);
        public readonly static BuildingType HomeChalet = new BuildingType(Codes.HOME_CHALET, "Chalet", BuildingCategory.Home);
        public readonly static BuildingType HomeVilla = new BuildingType(Codes.HOME_VILLA, "Villa", BuildingCategory.Home);
        public readonly static BuildingType HomeBungalow = new BuildingType(Codes.HOME_BUNGALOW, "Bungalow", BuildingCategory.Home);
        public readonly static BuildingType HomeFarm = new BuildingType(Codes.HOME_FARM, "Farm", BuildingCategory.Home);
        public readonly static BuildingType HomeGuestHouse = new BuildingType(Codes.HOME_GUESTHOUSE, "Guest House", BuildingCategory.Home);
        public readonly static BuildingType CampingGroundCampingGround = new BuildingType(Codes.CAMPINGGROUND_CAMPINGGROUND, "Camping Ground", BuildingCategory.CampingGround);
        public readonly static BuildingType YachtYacht = new BuildingType(Codes.YACHT_YACHT, "Yacht", BuildingCategory.Yacht);

        private readonly BuildingCategory _buildingCategory;
        public BuildingCategory BuildingCategory { get { return _buildingCategory; } }

        public BuildingType(string code, string description, BuildingCategory buildingCategory)
            : base(code, description)
        {
            _buildingCategory = buildingCategory;
        }

        public static List<BuildingType> GetBuildingTypesByCategory(BuildingCategory buildingCategory)
        {
            return Map.List.Where(x => x.BuildingCategory == buildingCategory).ToList();
        }

        public static BuildingType LookupByDescription(string description)
        {
            return Map.List.FirstOrDefault(item => item.Description.Equals(description, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
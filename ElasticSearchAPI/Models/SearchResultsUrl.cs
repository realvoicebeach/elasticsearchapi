﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class SearchResultsUrl
    {
        public const string DimensionsParameter = "N";
        public const string SortParameter = "Ns";
        public const string RefinementsParameter = "Ne";
        public const string SearchKeyParameter = "Ntk";
        public const string SearchTermsParameter = "Ntt";
        public const string ResultPageParameter = "resultPage";

        private readonly List<EndecaDimensionValue> _navigationDimensionValues = new List<EndecaDimensionValue>();

        public ReadOnlyCollection<EndecaDimensionValue> NavigationDimensionValues
        {
            get { return _navigationDimensionValues.AsReadOnly(); }
        }

        public void AddNavigationDimensionValue(EndecaDimensionValue dimensionValue)
        {
            if (!_navigationDimensionValues.Contains(dimensionValue))
            {
                _navigationDimensionValues.Add(dimensionValue);
            }
        }

        private readonly List<long> _navigationDimensionValueIds = new List<long>();

        public ReadOnlyCollection<long> NavigationDimensionValueIds
        {
            get { return _navigationDimensionValueIds.AsReadOnly(); }
        }

        public void AddNavigationDimensionValueId(long dimensionValueId)
        {
            if (!_navigationDimensionValueIds.Contains(dimensionValueId))
            {
                _navigationDimensionValueIds.Add(dimensionValueId);
            }
        }

        private readonly List<SortKey> _sortKeys = new List<SortKey>();

        public ReadOnlyCollection<SortKey> SortKeys
        {
            get { return _sortKeys.AsReadOnly(); }
        }

        public void AddSortKey(SortKey sortKey)
        {
            _sortKeys.Add(sortKey);
        }

        public string SearchKey { get; private set; }

        public string SearchTerm { get; private set; }

        public void SetKeywordSearch(string searchKey, string searchTerm)
        {
            if (string.IsNullOrEmpty(searchKey))
            {
                throw new ArgumentException("Argument cannot be null or empty.", "searchKey");
            }

            if (string.IsNullOrEmpty(searchTerm))
            {
                throw new ArgumentException("Argument cannot be null or empty.", "searchTerm");
            }

            SearchKey = searchKey;
            SearchTerm = searchTerm;
        }

        public void ClearKeywordSearch()
        {
            SearchKey = null;
            SearchTerm = null;
        }

        public bool HasKeywordSearch
        {
            get { return SearchKey != null; }
        }

        public int ResultPage { get; set; }

        public static SearchResultsUrl Parse(Uri url)
        {
            return Parse(new BrandedPath(url), HttpUtility.ParseQueryString(url.Query));
        }

        public static SearchResultsUrl Parse(string baseDomain, string path, NameValueCollection queryString)
        {
            return Parse(new BrandedPath(baseDomain, path), queryString);
        }

        public static SearchResultsUrl Parse(BrandedPath searchPath, NameValueCollection queryString)
        {
            var result = new SearchResultsUrl();

            int dimensionValueIdPathPartIndex = GetIndexOfDimensionValueIdPathPart(searchPath);

            if (dimensionValueIdPathPartIndex != -1)
            {
                AddDimensionValueIdsToSearchUrl(result, searchPath.PathParts[dimensionValueIdPathPartIndex]);
            }

            AddNameValueParametersToSearchUrl(result, searchPath, dimensionValueIdPathPartIndex);

            ParseResultPage(result, queryString);
            ParseSortKeys(result, queryString);
            ParseSearchTerms(result, queryString);
            ParseNamedDimensionParameters(result, queryString);
            ParseDimensionIds(result, queryString);

            return result;
        }

        private static void ParseResultPage(SearchResultsUrl result, NameValueCollection queryString)
        {
            string resultPage = queryString[ResultPageParameter];
            result.ResultPage = !string.IsNullOrEmpty(resultPage) ? resultPage.ToInt() : 0;
        }

        private static void ParseSortKeys(SearchResultsUrl result, NameValueCollection queryString)
        {
            string sort = queryString[SortParameter];
            if (!string.IsNullOrEmpty(sort))
            {
                //There are links cached in Google that contain our old p_DOLU property
                //That property has been removed from Endeca and when we pass it in, Endeca
                //throws an error, which causes the user to be redirected to the home page.
                //Stripping the parameter out of the sort string should prevent that and keep
                //a lot of noise out of our event log.
                sort = sort.Replace("p_DOLU|1", "").Replace("p_DOLU|0", "");

                List<SortKey> sortKeys = SortKey.ParseList(sort);
                foreach (SortKey key in sortKeys)
                {
                    result.AddSortKey(key);
                }
            }
        }

        private static void ParseSearchTerms(SearchResultsUrl result, NameValueCollection queryString)
        {
            string searchKey = queryString[SearchKeyParameter];
            searchKey = string.IsNullOrEmpty(searchKey) ? null : searchKey;

            string searchTerms = queryString[SearchTermsParameter];
            searchTerms = string.IsNullOrEmpty(searchTerms) ? null : string.Join(" ", searchTerms.Split(' ').Distinct().ToArray());
            if (!string.IsNullOrEmpty(searchKey) && !string.IsNullOrEmpty(searchTerms))
            {
                result.SetKeywordSearch(searchKey, searchTerms);
            }
        }

        private static void ParseNamedDimensionParameters(SearchResultsUrl result, NameValueCollection queryString)
        {
            var endecaParameterNames = new List<string>
            {
                DimensionsParameter,
                SortParameter,
                RefinementsParameter,
                SearchKeyParameter,
                SearchTermsParameter
            };

            IEnumerable<string> genericParameterNames = queryString.OfType<string>()
                .Except(endecaParameterNames);

            foreach (string parameterName in genericParameterNames)
            {
                EndecaDimension dimension = EndecaDimension.LookupByUrlParameter(parameterName);

                if (dimension != null)
                {
                    string[] dimensionValues = queryString[parameterName].Split(new[]
                    {
                        ","
                    }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string dimensionValue in dimensionValues)
                    {
                        result.AddNavigationDimensionValue(new EndecaDimensionValue(dimension, dimensionValue));
                    }
                }
            }
        }

        private static void ParseDimensionIds(SearchResultsUrl result, NameValueCollection queryString)
        {
            string dimensions = queryString[DimensionsParameter];

            IEnumerable<string> dimensionIds = string.IsNullOrEmpty(dimensions)
                                                   ? Enumerable.Empty<string>()
                                                   : dimensions.Split(' ').Distinct();

            foreach (string dimensionId in dimensionIds)
            {
                result.AddNavigationDimensionValueId(long.Parse(dimensionId));
            }
        }

        private static void AddNameValueParametersToSearchUrl(SearchResultsUrl result, BrandedPath searchPath, int dimensionValueIdPathPartIndex)
        {
            for (int i = dimensionValueIdPathPartIndex + 1; i < searchPath.PathParts.Count; i++)
            {
                if (searchPath.PathParts[i].Contains("_"))
                {
                    UrlParameter parameter = UrlParameter.Parse(searchPath.PathParts[i]);

                    EndecaDimension dimension = EndecaDimension.LookupByUrlParameter(parameter.Name);

                    if (dimension == null)
                    {
                        continue;
                    }

                    foreach (UrlParameterValue value in parameter.Values)
                    {
                        result.AddNavigationDimensionValue(new EndecaDimensionValue(dimension, value.ToString()));
                    }
                }
            }
        }

        private static void AddDimensionValueIdsToSearchUrl(SearchResultsUrl result, string dimensionValueIdPathPart)
        {
            string delimitedIds = dimensionValueIdPathPart.TrimStart('n');

            string[] ids = delimitedIds.Split('-');

            foreach (string id in ids)
            {
                result.AddNavigationDimensionValueId(long.Parse(id));
            }
        }

        private static int GetIndexOfDimensionValueIdPathPart(BrandedPath searchPath)
        {
            int dimensionValueIdPathPartIndex = -1;

            var dimensionValueIdPathPartRegex = new Regex(@"^n\d+(\-\d+)*$");

            for (int i = 0; i < searchPath.PathParts.Count; i++)
            {
                if (dimensionValueIdPathPartRegex.IsMatch(searchPath.PathParts[i]))
                {
                    dimensionValueIdPathPartIndex = i;
                    break;
                }
            }
            return dimensionValueIdPathPartIndex;
        }
    }
}
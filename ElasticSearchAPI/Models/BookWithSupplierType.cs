﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class BookWithSupplierType : CodeValue<BookWithSupplierType>
    {
        public static class Codes
        {
            public const string DIRECT = "DIRECT";
            public const string VIA_EXTRANET = "VIA_EXTRANET";
        }

        public static readonly BookWithSupplierType Direct = new BookWithSupplierType(Codes.DIRECT, "Direct");
        public static readonly BookWithSupplierType ViaExtranet = new BookWithSupplierType(Codes.VIA_EXTRANET, "Via Extranet");

        private BookWithSupplierType(string code, string description)
            : base(code, description)
        {
        }
    }
}
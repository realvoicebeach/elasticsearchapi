﻿using System;
using System.Linq;

namespace ElasticSearchAPI.Models
{
    public static class ObjectUtilities
    {
        public static int CombineHashCodes(int h1, int h2)
        {
            return ((h1 << 5) + h1) ^ h2;
        }

        public static int CombineHashCodes(int h1, params int[] hashes)
        {
            return hashes.Aggregate(h1, CombineHashCodes);
        }

        public static DateTime ToDateTimeDefaultWhenEmpty(this object obj)
        {
            var stringValue = obj.ToString();
            DateTime value;
            DateTime.TryParse(stringValue, out value);

            return value;
        }

        public static decimal ToDecimalDefaultWhenEmpty(this object obj)
        {
            var stringValue = obj.ToString();
            decimal value;
            Decimal.TryParse(stringValue, out value);

            return value;
        }

        public static bool IsNull(this object obj)
        {
            return obj == null;
        }
    }
}
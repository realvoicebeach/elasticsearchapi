﻿using ElasticSearchAPI.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class FloorPlan : IFloorPlan
    {
        private readonly FloorPlanType _floorPlanType;
        public FloorPlanType FloorPlanType { get { return _floorPlanType; } }

        private readonly BedroomType _bedrooms;
        public BedroomType Bedrooms { get { return _bedrooms; } }

        protected readonly List<string> _bonusRooms = new List<string>();
        public ReadOnlyCollection<string> BonusRooms { get { return _bonusRooms.AsReadOnly(); } }

        public FloorPlan(FloorPlanType floorPlanType, BedroomType bedrooms, IEnumerable<string> bonusRooms)
        {
            _floorPlanType = floorPlanType;
            _bedrooms = bedrooms;
            _bonusRooms.AddRange(bonusRooms);
        }

        public string FloorPlanDescription
        {
            get
            {
                string bedroomType = Bedrooms != null ? Bedrooms.Description : string.Empty;
                string bonusBedrooms = string.Empty;
                if (BonusRooms.Any())
                {
                    bonusBedrooms = BonusRooms.Aggregate(bonusBedrooms, (current, bonusRoom) => !String.IsNullOrEmpty(current) ? string.Format("{0} + {1}", current, bonusRoom) : bonusRoom);
                }
                var floorPlanType = FloorPlanType != null ? FloorPlanType.Description : string.Empty;
                var resultDescription = string.Empty;

                if ((String.IsNullOrEmpty(bedroomType) && !String.IsNullOrEmpty(floorPlanType)) || bedroomType == floorPlanType)
                {
                    resultDescription = String.IsNullOrEmpty(bonusBedrooms) ? floorPlanType : String.Format("{0} + {1}", floorPlanType, bonusBedrooms);
                }
                else if (!String.IsNullOrEmpty(bedroomType) && !String.IsNullOrEmpty(floorPlanType))
                {
                    if (bedroomType.Contains(floorPlanType) || _bedrooms.Equals(BedroomType.DormRoom))
                    {
                        resultDescription = String.IsNullOrEmpty(bonusBedrooms) ? bedroomType : String.Format("{0} + {1}", bedroomType, bonusBedrooms);
                    }
                    else
                        resultDescription = String.IsNullOrEmpty(bonusBedrooms) ? String.Format("{0} {1}", bedroomType, floorPlanType) : String.Format("{0} + {1} {2}", bedroomType, bonusBedrooms, floorPlanType);
                }

                return resultDescription;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is FloorPlan))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            var value = (FloorPlan)obj;
            return FloorPlanType.Equals(value.FloorPlanType) && Bedrooms.Equals(value.Bedrooms) && BonusRooms.Count.Equals(value.BonusRooms.Count);
        }

        public override int GetHashCode()
        {
            int h1 = string.IsNullOrEmpty(FloorPlanType.Description) ? 0 : FloorPlanType.Description.GetHashCode();
            int h2 = BonusRooms.Any() ? BonusRooms.GetHashCode() : 0;
            int h3 = string.IsNullOrEmpty(Bedrooms.Description) ? 0 : Bedrooms.Description.GetHashCode();
            return ObjectUtilities.CombineHashCodes(h1, h2, h3);
        }

        public static bool operator ==(FloorPlan lhs, FloorPlan rhs)
        {
            if (ReferenceEquals(lhs, null) && ReferenceEquals(rhs, null))
            {
                return true;
            }
            if (ReferenceEquals(lhs, null))
            {
                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(FloorPlan lhs, FloorPlan rhs)
        {
            return !(lhs == rhs);
        }
    }
}
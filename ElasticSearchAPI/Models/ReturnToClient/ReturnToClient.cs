﻿using System.Collections.Generic;

namespace ElasticSearchAPI.Models.ReturnToClient
{
    public class Refinement
    {
        public string name { get; set; }
        public int count { get; set; }
        public string urlValue { get; set; }
    }

    public class RefinementDimensions
    {
        public string name { get; set; }
        public int id { get; set; }
        public List<Refinement> refinements { get; set; }
    }

    public class Count
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }

    public class ReturnToLegoLand
    {
        public List<PropertyInfo> featuredProperties { get; set; }
        public List<PropertyInfo> propertyInfos { get; set; }
        public List<RefinementDimensions> refinementDimensions { get; set; }
        public List<RefinementDimensions> currentDimensions { get; set; }
        public int TotalRecords { get; set; }
    }
}
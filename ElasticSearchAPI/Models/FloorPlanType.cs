﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class FloorPlanType : CodeValue<FloorPlanType>
    {
        private static readonly Dictionary<string, FloorPlanType> _mapBydescription = new Dictionary<string, FloorPlanType>();

        public static class Codes
        {
            public const string HOTEL_ROOM = "Hotel Room";
            public const string SUITE = "Suite";
            public const string FLAT_APARTMENT = "Flat / Apartment";
            public const string STUDIO = "Studio";
            public const string CONDO = "Condo";
            public const string TOWNHOME = "Townhome";
            public const string HOME = "Home";
            public const string PENTHOUSE = "Penthouse";
            public const string CONDO_APARTMENT = "Apartment";
            public const string LOFT = "Loft";
            public const string CAMPING_GROUND = "Camping Ground";
            public const string YACHT = "Yacht";
        }

        public readonly static FloorPlanType HotelRoom = new FloorPlanType(Codes.HOTEL_ROOM, "Hotel Room", FloorPlanCategory.HotelRoom);
        public readonly static FloorPlanType Suite = new FloorPlanType(Codes.SUITE, "Suite", FloorPlanCategory.Suite);
        public readonly static FloorPlanType Penthouse = new FloorPlanType(Codes.PENTHOUSE, "Penthouse", FloorPlanCategory.Suite);
        public readonly static FloorPlanType Studio = new FloorPlanType(Codes.STUDIO, "Studio", FloorPlanCategory.Studio);
        public readonly static FloorPlanType FlatApartment = new FloorPlanType(Codes.FLAT_APARTMENT, "Flat / Apartment", FloorPlanCategory.Studio);
        public readonly static FloorPlanType Condo = new FloorPlanType(Codes.CONDO, "Condo", FloorPlanCategory.Condo);
        public readonly static FloorPlanType CondoApartment = new FloorPlanType(Codes.CONDO_APARTMENT, "Apartment", FloorPlanCategory.Condo);
        public readonly static FloorPlanType Loft = new FloorPlanType(Codes.LOFT, "Loft", FloorPlanCategory.Condo);
        public readonly static FloorPlanType Townhome = new FloorPlanType(Codes.TOWNHOME, "Townhome", FloorPlanCategory.Townhome);
        public readonly static FloorPlanType Home = new FloorPlanType(Codes.HOME, "Home", FloorPlanCategory.Home);
        public readonly static FloorPlanType CampingGround = new FloorPlanType(Codes.CAMPING_GROUND, "Camping Ground", FloorPlanCategory.CampingGround);
        public readonly static FloorPlanType Yacht = new FloorPlanType(Codes.YACHT, "Yacht", FloorPlanCategory.Yacht);

        public FloorPlanCategory FloorPlanTypeCategory { get; private set; }

        private FloorPlanType(string code, string description, FloorPlanCategory floorPlanCategory) : base(code, description)
        {
            _mapBydescription.Add(description, this);
            FloorPlanTypeCategory = floorPlanCategory;
        }

        public static FloorPlanType FindFloorPlanType(string description)
        {
            return _mapBydescription.ContainsKey(description) ? _mapBydescription[description] : null;
        }

        public static List<FloorPlanType> GetFloorPlanTypesByCategory(FloorPlanCategory floorPlanCategory)
        {
            return Map.List.Where(x => x.FloorPlanTypeCategory == floorPlanCategory).ToList();
        }

    }
}
﻿using System;
using System.Collections.Generic;

namespace ElasticSearchAPI.Models
{
    public class LazyList<T>
    {
        private readonly List<T> _list = new List<T>();

        public List<T> List
        {
            get
            {
                if (_lazyLoadFunction != null)
                {
                    _list.AddRange(_lazyLoadFunction());
                    _lazyLoadFunction = null;
                }
                return _list;
            }
        }

        public bool AreLoaded
        {
            get { return _lazyLoadFunction == null; }
        }

        private Func<IEnumerable<T>> _lazyLoadFunction;

        public void AddLazyLoad(IList<T> lazyLoadList)
        {
            AddLazyLoad(() => lazyLoadList);
        }

        public void AddLazyLoad(IEnumerable<T> lazyLoadEnumeration)
        {
            AddLazyLoad(() => lazyLoadEnumeration);
        }

        public void AddLazyLoad(Func<IEnumerable<T>> lazyLoadFunction)
        {
            _lazyLoadFunction = lazyLoadFunction;
        }
    }
}
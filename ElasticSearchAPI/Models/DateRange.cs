﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    [Serializable]
    public class DateRange : IEnumerable<DateTime>
    {
        private readonly DateTime _begin;
        private readonly DateTime _end;

        public DateRange(DateTime begin, DateTime end)
        {
            if (end.Date < begin.Date)
            {
                throw new ArgumentException("Argument end must be later than argument begin.");
            }
            _begin = begin.Date;
            _end = end.Date;
        }

        public static DateRange Merge(DateRange range1, DateRange range2)
        {
            DateTime earliestBegin = range1.Begin <= range2.Begin ? range1.Begin : range2.Begin;
            DateTime latestEnd = range1.End >= range2.End ? range1.End : range2.End;
            return new DateRange(earliestBegin, latestEnd);
        }

        public DateTime Begin
        {
            get
            {
                return _begin;
            }
        }

        public DateTime End
        {
            get
            {
                return _end;
            }
        }

        public int NumDays
        {
            get
            {
                return End.Subtract(Begin).Days + 1;
            }
        }

        public bool Overlaps(DateRange range)
        {
            return Begin <= range.Begin && range.Begin <= End
                || range.Begin <= Begin && Begin <= range.End;
        }

        public bool Overlaps(DateTime? begin, DateTime? end)
        {
            if (begin.HasValue && end.HasValue)
            {
                return Overlaps(new DateRange(begin.Value, end.Value));
            }

            if (!begin.HasValue && !end.HasValue)
            {
                return true;
            }

            if (begin.HasValue)
            {
                return begin <= End;
            }

            return end >= Begin;
        }

        public bool Contains(DateTime date)
        {
            date = date.Date;
            return Begin <= date && date <= End;
        }

        public bool Contains(DateRange dateRange)
        {
            return Contains(dateRange.Begin) && Contains(dateRange.End);
        }

        public DateRange Intersect(DateRange dateRange)
        {
            if (!Overlaps(dateRange))
            {
                throw new InvalidOperationException(string.Format("Cannot create the intersection because the parameter date range {0} does not overlap this date range {1}.", dateRange, this));
            }

            //I want to use the latest begin date and the earliest end date
            DateTime begin = dateRange.Begin > Begin ? dateRange.Begin : Begin;
            DateTime end = dateRange.End < End ? dateRange.End : End;

            return new DateRange(begin, end);
        }

        public DateRange GetMinimalSuperset(DateRange dateRange)
        {
            //I want to use the earliest begin date and the latest end date
            DateTime begin = dateRange.Begin < Begin ? dateRange.Begin : Begin;
            DateTime end = dateRange.End > End ? dateRange.End : End;

            return new DateRange(begin, end);
        }

        public IEnumerable<DateRange> Subtract(DateRange dateRange)
        {
            return Subtract(new[] { dateRange });
        }

        public IEnumerable<DateRange> Subtract(IEnumerable<DateRange> dateRanges)
        {
            DateTime begin = Begin;
            foreach (var dateRange in dateRanges.OrderBy(x => x.Begin))
            {
                if (!Overlaps(dateRange))
                {
                    throw new ArgumentException(string.Format("Cannot subtract date ranges because the parameter date range {0} does not overlap this date range {1}.", dateRange, this), "dateRanges");
                }

                if (begin < dateRange.Begin)
                {
                    yield return new DateRange(begin, dateRange.Begin.AddDays(-1));
                }
                begin = dateRange.End.AddDays(1);
            }

            if (begin <= End)
            {
                yield return new DateRange(begin, End);
            }
        }

        public IEnumerable<DateRange> Union(DateRange dateRange)
        {
            IEnumerable<DateRange> ranges;
            if (!Overlaps(dateRange))
            {
                ranges = new[] { this }.And(dateRange);
            }
            else
            {
                ranges = new[] { Merge(this, dateRange) };
            }
            return ranges.OrderBy(x => x.Begin);
        }

        public IEnumerable<DateRange> UnionDistinctRanges(DateRange dateRange)
        {
            IEnumerable<DateRange> ranges;
            if (!Overlaps(dateRange))
            {
                ranges = new[] { this }.And(dateRange);
            }
            else
            {
                ranges = Subtract(dateRange).And(Intersect(dateRange));
            }
            return ranges.OrderBy(x => x.Begin);
        }

        public bool IsAdjacentTo(DateRange dateRange)
        {
            return !Overlaps(dateRange) && (End.AddDays(1) == dateRange.Begin || Begin.AddDays(-1) == dateRange.End);
        }

        public IEnumerator<DateTime> GetEnumerator()
        {
            DateTime currentDate = Begin;
            while (currentDate <= End)
            {
                yield return currentDate;
                currentDate = currentDate.AddDays(1);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            var other = (DateRange)obj;
            return Begin == other.Begin && End == other.End;
        }

        public override int GetHashCode()
        {
            return ObjectUtilities.CombineHashCodes(Begin.GetHashCode(), End.GetHashCode());
        }

        public static bool operator ==(DateRange lhs, DateRange rhs)
        {
            if (ReferenceEquals(lhs, null) && ReferenceEquals(rhs, null))
            {
                return true;
            }
            if (ReferenceEquals(lhs, null))
            {
                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(DateRange lhs, DateRange rhs)
        {
            return !(lhs == rhs);
        }

        public override string ToString()
        {
            return string.Format("{0:d} - {1:d}", Begin, End);
        }

        public static DateRange Parse(string begin, string end)
        {
            DateTime? beginDate = begin.ToDateTimeNullWhenEmpty();
            DateTime? endDate = end.ToDateTimeNullWhenEmpty();

            DateRange result = null;
            if (beginDate.HasValue && endDate.HasValue && beginDate <= endDate)
            {
                result = new DateRange(beginDate.Value, endDate.Value);
            }
            return result;
        }

        public static bool IsValid(string begin, string end)
        {
            bool result = false;
            try
            {
                DateTime? beginDate = begin.ToDateTimeNullWhenEmpty();
                DateTime? endDate = end.ToDateTimeNullWhenEmpty();

                DateRange validRange = null;
                if (beginDate.HasValue && endDate.HasValue && beginDate < endDate)
                {
                    validRange = new DateRange(beginDate.Value, endDate.Value);
                }
                if (validRange != null)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        public IEnumerable<DateTime> GetDatesByDayOfWeek(DayOfWeek dayOfWeek)
        {
            return GetDatesByDayOfWeek(new List<DayOfWeek> { dayOfWeek });
        }

        public IEnumerable<DateTime> GetDatesByDayOfWeek(IEnumerable<DayOfWeek> daysOfWeek)
        {
            return this.Where(x => daysOfWeek.Contains(x.DayOfWeek));
        }

        public static IEnumerable<DateRange> CreateFromDates(IEnumerable<DateTime> unavailableNights)
        {
            DateRange range = null;
            foreach (var day in unavailableNights.Select(x => x.Date).OrderBy(x => x).Distinct())
            {
                if (range == null)
                {
                    range = new DateRange(day, day);
                }
                else if (day == range.End.AddDays(1))
                {
                    range = new DateRange(range.Begin, day);
                }
                else
                {
                    yield return range;
                    range = new DateRange(day, day);
                }
            }
            if (range != null)
            {
                yield return range;
            }
        }
    }

    public static class DateRangeUtils
    {
        public static DateRange MinimalSuperset(this IEnumerable<DateRange> source)
        {
            return new DateRange(source.Min(x => x.Begin), source.Max(x => x.End));
        }
    }
}
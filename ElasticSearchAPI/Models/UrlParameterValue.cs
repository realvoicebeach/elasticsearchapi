﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public class UrlParameterValue
    {
        private readonly string _value;

        private static readonly List<Pair<string>> SpecialChars = new List<Pair<string>>
        {
            new Pair<string>("é", "e"), //Used in a Canadian town
            new Pair<string>("ñ", "n"), //Las Leñas
            new Pair<string>("ā", "a"), //Hawaiian
            new Pair<string>("ē", "e"), //Hawaiian
            new Pair<string>("ī", "i"), //Hawaiian
            new Pair<string>("ō", "o"), //Hawaiian
            new Pair<string>("ū", "u"), //Hawaiian
            new Pair<string>("ʻ", ""), //Hawaiian
        };

        private static string ReplaceSpecialCharsWithEnglishChars(string value)
        {
            foreach (Pair<string> specialChar in SpecialChars)
            {
                value = value.Replace(specialChar.First, specialChar.Second);
            }

            return value;
        }

        public UrlParameterValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException("Argument cannot be null or empty.", "value");
            }

            string temp = Regex.Replace(ReplaceSpecialCharsWithEnglishChars(value.ToLower()), @"[^a-z0-9]", "-"); //Replace all special chars with dash
            temp = temp.Trim('-'); //Remove any leading or trailing dashes
            _value = Regex.Replace(temp, @"\-+", "-"); //Replace any repeating dashes with a single dash
        }

        /// <summary>
        /// DisplayValue is only an approximation of what the string would have been before
        /// converting it into a url parameter value. We convert every special character into
        /// a dash so when we replace dashes with spaces, some things that were not spaces
        /// will now be spaces
        /// </summary>
        public string DisplayValue
        {
            get
            {
                TextInfo textFormat = new CultureInfo("en-US", false).TextInfo;
                return textFormat.ToTitleCase(_value.Replace("-", " "));
            }
        }

        public override string ToString()
        {
            return _value;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var other = (UrlParameterValue)obj;

            return _value == other._value;
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public static bool operator ==(UrlParameterValue lhs, UrlParameterValue rhs)
        {
            if (ReferenceEquals(lhs, null) && ReferenceEquals(rhs, null))
            {
                return true;
            }
            if (ReferenceEquals(lhs, null))
            {
                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(UrlParameterValue lhs, UrlParameterValue rhs)
        {
            return !(lhs == rhs);
        }

    }
}
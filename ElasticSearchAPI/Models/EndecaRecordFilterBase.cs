﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchAPI.Models
{
    public abstract class EndecaRecordFilterBase
    {
        public string Name { get; set; }

        public abstract string GetFilterString();

        public override string ToString()
        {
            return GetFilterString();
        }

        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var other = ((EndecaRecordFilterBase)obj);

            return Equals(GetFilterString(), other.GetFilterString()) && Equals(Name, other.Name);
        }

        public override int GetHashCode()
        {
            return ObjectUtilities.CombineHashCodes(GetFilterString().GetHashCode(), Name.GetHashCode());
        }
    }
}
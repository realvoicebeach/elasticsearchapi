﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// For requesting geography locations, used for earching. Can be used for specific inventory IDs, or in the absence of those, all geography locations will be returned.
    /// </summary>
    public class GeographyRequest
    {
        /// <summary>
        /// Authentication for the Realvoice API
        /// </summary>
        [Required]
        public Authentication Authentication { get; set; }
    }
}
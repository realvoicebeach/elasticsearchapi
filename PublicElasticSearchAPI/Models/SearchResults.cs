﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PublicElasticSearchAPI.Models
{
    /// <summary>
    /// The search result returned to the user.
    /// </summary>
    public class SearchResults
    {
        /// <summary>
        /// Constructor for SearchResults
        /// </summary>
        public SearchResults()
        {
            Inventories = new List<PropertyInfo>();
            FeaturedInventories = new List<PropertyInfo>();
        }

        /// <summary>
        /// A list of inventory IDs that match the search criteria.
        /// </summary>
        public List<PropertyInfo> Inventories { get; set; }
        /// <summary>
        /// a list of inventory IDs that are featured and that match the search criteria.
        /// </summary>
        public List<PropertyInfo> FeaturedInventories { get; set; }
        /// <summary>
        /// A list of amenities and their corresponding inventory count match, associated with the search criteria.
        /// </summary>
        public Dictionary<string, int> Amenities { get; set; }
        /// <summary>
        /// A list of destinations and their corresponding inventory count match, associated with the search criteria.
        /// </summary>
        public Dictionary<string, int> Destinations { get; set; }
        /// <summary>
        /// A list of cities and their corresponding inventory count match, associated with the search criteria.
        /// </summary>
        public Dictionary<string, int> Cities { get; set; }
        /// <summary>
        /// A list of states and their corresponding inventory count match, associated with the search criteria.
        /// </summary>
        public Dictionary<string, int> States { get; set; }
        /// <summary>
        /// A list of countries and their corresponding inventory count match, associated with the search criteria.
        /// </summary>
        public Dictionary<string, int> Countries { get; set; }
        /// <summary>
        /// A list of speeling availability and associated inventory count match, associated with the search criteria.
        /// </summary>
        public Dictionary<string, int> Sleeps { get; set; }
        /// <summary>
        /// Returns the total number of search results
        /// </summary>
        public int TotalResults { get; set; }
    }
}
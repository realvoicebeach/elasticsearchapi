﻿using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Geography;
using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Models.Responses;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    /// <summary>
    /// Returns information related to destinations
    /// </summary>
    public class GetDestinationListController : ApiController
    {
        /// <summary>
        /// Returns a destination ID and Destination pair, based on the input
        /// </summary>
        /// <param name="destReq">The destination request</param>
        /// <returns>an ID/Destination pair</returns>
        public Dictionary<long, string> Post([FromBody]DestinationRequest destReq)
        {
            GeographyQuery geographyQuery = new GeographyQuery();
            RentalUnitContentQuery rentalUnitContentQuery = new RentalUnitContentQuery();

            ElasticGeographySearchResult geoHits = geographyQuery.GetGeographiesByDestination(destReq.Destination);

            Dictionary<long, string> destDetails = new Dictionary<long, string>();

            //foreach (GeographyHit hit in destinations)
            if(geoHits.hits.hits.Length > 0)
            {
                ElasticRentalUnitContentSearchResults result = rentalUnitContentQuery.GetRentalUnitContentByInventoryID(geoHits.hits.hits[0]._source.InventoryID);

                if (result != null)
                {
                    if (result.hits.hits != null)
                    {
                        if (result.hits.hits[0]._source.DestinationId != null)
                        {
                            try
                            {
                                destDetails.Add(result.hits.hits[0]._source.DestinationId == null ? 0 : (long)result.hits.hits[0]._source.DestinationId, geoHits.hits.hits[0]._source.Destination);
                            }
                            catch (Exception)
                            { }
                        }
                    }
                }
            }

            return destDetails;
        }
    }
}

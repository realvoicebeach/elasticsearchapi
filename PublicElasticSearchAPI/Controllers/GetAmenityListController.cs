﻿using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Amenities;
using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Models.Responses.Amenities;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    /// <summary>
    /// Returns a list of amenities that can be used for searches.
    /// </summary>
    /*public class GetAmenityListController : ApiController
    {
        // POST: api/GetAmenityList
        /// <summary>
        /// Gets a full list of amenities that can be used for querying Realvoice's API for properties
        /// </summary>
        /// <param name="amenityRequest">The request for amenities</param>
        public List<KeyValuePair<string, int>> Post([FromBody]FullAmenityRequest amenityRequest)
        {
            AmenityQuery amenityQuery = new AmenityQuery();

            List<SplurgeAmenityHit> splurgeAmenities = amenityQuery.GetSplurgeAmenities();
            List<AmenityHit> amenityHits = amenityQuery.GetAllAmenities();

            List<KeyValuePair<string, int>> returnAmenities = new List<KeyValuePair<string, int>>();

            foreach (SplurgeAmenityHit s in splurgeAmenities)
            {
                int count = (from x in amenityHits
                                where x._source.AttribName == s._source.AmenityName
                                select x).Count();

                returnAmenities.Add(new KeyValuePair<string, int>(s._source.AmenityName, count));
            }

            returnAmenities = (from x in returnAmenities
                                orderby x.Value descending
                                select x).ToList();

            return returnAmenities;
        }
    }*/
}

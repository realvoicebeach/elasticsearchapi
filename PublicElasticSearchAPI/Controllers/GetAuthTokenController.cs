﻿using PublicElasticSearchAPI.Controllers.Helper;
using PublicElasticSearchAPI.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace PublicElasticSearchAPI.Controllers
{
    public class GetAuthTokenController : ApiController
    {
        private PluginDbContext dbkey = new PluginDbContext();

        [HttpPost]
        public IHttpActionResult Authenticate([FromBody]PluginKeyDomain login)
        {
            var loginResponse = new AuthResponse { };

            HttpResponseMessage responseMsg = new HttpResponseMessage();
            bool isUsernamePasswordValid = false;

            if (login != null)
            {
                if (Request.Headers.UserAgent != null)
                {
                    var req = Request.Headers.UserAgent.ToString();
                    if (req != String.Empty && req.Split(';').Length > 0)
                    {
                        login.PluginDomain = req.Split(';')[1].Trim();
                    }
                }
                else
                {
                    login.PluginDomain = null;
                }
                isUsernamePasswordValid = verifyKeyAndDomain(login);
            }

            // if credentials are valid
            if (isUsernamePasswordValid)
            {
                string token = createToken(login.PluginDomain);
                loginResponse.Status = "1";
                loginResponse.Message = "Success";
                loginResponse.Token = token;
                //return the token
                return Ok(loginResponse);
            }
            else
            {
                // if credentials are not valid send unauthorized status code in response
                loginResponse.Status = "0";
                responseMsg.StatusCode = HttpStatusCode.Unauthorized;
                loginResponse.Message = ResponseMessage(responseMsg).ToString();
                return Ok(loginResponse); 
            }
        }

        private string createToken(string username)
        {
            //Set issued at date
            DateTime issuedAt = DateTime.UtcNow;
            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddDays(7);

            var tokenHandler = new JwtSecurityTokenHandler();

            //create a identity and add claims to the user which we want to log in
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, username)
            });

            const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);


            //create the jwt
            var token =
                (JwtSecurityToken)
                    tokenHandler.CreateJwtSecurityToken(issuer: "https://qa-realsync.realvoice.com", audience: "https://qa-realsync.realvoice.com",
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }

        private bool verifyKeyAndDomain(PluginKeyDomain details)
        {
            Helpers _help = new Helpers();
            var dbkeys = dbkey.PluginKeyDomains.ToList();
            var _idx = dbkeys.FindIndex(x => x.PluginKEY == details.PluginKEY && _help.Decrypt(x.PluginDomain, "Real Voice") == details.PluginDomain);
            return _idx >= 0 ? true : false;
        }
    }
}

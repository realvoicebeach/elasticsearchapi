﻿using PublicElasticSearchAPI.Models;
using System;
using System.Linq;
using System.Web.Http;
using PublicElasticSearchAPI.Controllers.Helper;

namespace PublicElasticSearchAPI.Controllers
{
    public class UpdateDomainController : ApiController
    {
        private PluginDbContext dbkey = new PluginDbContext();
        Helpers _help = new Helpers();
        // POST api/<controller>
        public AuthResponse Post([FromBody]PluginKeyDomain value)
        {
            var response = new AuthResponse { };
            try
            {
                int noOfPlugins = dbkey.PluginKeyDomains.Count(x => x.PluginKEY == value.PluginKEY);
                if (noOfPlugins == 1)
                {
                    var plugin = dbkey.PluginKeyDomains.Single(x => x.PluginKEY == value.PluginKEY);
                    if (plugin.PluginDomain != null || plugin.PluginDomain != String.Empty) {
                        plugin.PluginDomain = _help.Encrypt(value.PluginDomain, "Real Voice");
                        dbkey.SaveChanges();
                        response.Status = "1";
                        response.Message = "Success";
                        return response;
                    }
                    else
                    {
                        response.Status = "0";
                        response.Message = "Domain not registered / activated";
                        return response;
                    }
                }
                else
                {
                    response.Status = "0";
                    response.Message = "Invalid Key";
                    return response;
                }
            }
            catch
            {
                response.Status = "0";
                response.Message = "An error has ocurred";
                return response;
            }
        }
    }
}
﻿using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Amenities;
using ElasticSearch.Models.Responses.Geography;
using ElasticSearch.Models.Responses.NightlyAverage;
using ElasticSearch.Models.Responses.Properties;
using ElasticSearch.Models.Responses.RentalUnitContent;
using ElasticSearchCore.API.ElasticQueries;
using ElasticSearchCore.Models.Responses;
using ElasticSearchCore.Models.Responses.Amenities;
using PublicElasticSearchAPI.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PublicElasticSearchAPI.Controllers.Helper
{
    /// <summary>
    /// A class to help controllers process requests
    /// </summary>
    public class Helpers
    {
        /// <summary>
        /// Gets search refinements available, based in supplied inventoryIDs.
        /// </summary>
        /// <param name="sortedProperties">The propertyHit list of inventories to be used</param>
        /// <returns></returns>
        public List<Dictionary<string, int>> GetSearchRefinements(List<PropertyHit> sortedProperties)
        {
            List<long> inventoryIDs = (from x in sortedProperties
                                       select x._source.InventoryID).ToList();

            Dictionary<string, int> amenities = new Dictionary<string, int>();
            Dictionary<string, int> destinations = new Dictionary<string, int>();
            Dictionary<string, int> cities = new Dictionary<string, int>();
            Dictionary<string, int> states = new Dictionary<string, int>();
            Dictionary<string, int> countries = new Dictionary<string, int>();
            Dictionary<string, int> sleeps = new Dictionary<string, int>();

            Task[] tasks = new Task[6];

            tasks[0] = Task.Run(() => { amenities = GetAmenitiesRefinements(inventoryIDs); });
            tasks[1] = Task.Run(() => { destinations = GetDestinationRefinements(inventoryIDs); });
            tasks[2] = Task.Run(() => { cities = GetCityRefinements(inventoryIDs); });
            tasks[3] = Task.Run(() => { states = GetStateRefinements(inventoryIDs); });
            tasks[4] = Task.Run(() => { countries = GetCountryRefinements(inventoryIDs); });
            tasks[5] = Task.Run(() => { sleeps = GetSleepsRefinements(inventoryIDs); });

            Task.WaitAll(tasks);

            return new List<Dictionary<string, int>>()
            {
                amenities,
                destinations,
                cities,
                states,
                countries,
                sleeps
            };
        }

        /// <summary>
        /// Gets a dictionary of amenity names and number of inventories corresponding to the supplied list of inventory IDs.
        /// </summary>
        /// <param name="inventoryIDs">The list of inventory IDs to search by</param>
        /// <returns></returns>
        public Dictionary<string, int> GetAmenitiesRefinements(List<long> inventoryIDs)
        {
            AmenityQuery amenityQuery = new AmenityQuery();
            ElasticAmenitySearchResult elasticAmenitySearchResult = amenityQuery.GetAmenitiesByInventoryIDs(inventoryIDs);
            List<SplurgeAmenityHit> splurgeAmenities = amenityQuery.GetSplurgeAmenities();
            ConcurrentDictionary<string, int> results = new ConcurrentDictionary<string, int>();

            List<Task> tasks = new List<Task>();

            foreach(SplurgeAmenityHit splurgeAmenityHit in splurgeAmenities)
            {
                tasks.Add(Task.Run(() => { 
                List<AmenityHit> hits = (from x in elasticAmenitySearchResult.hits.hits
                                   where x._source.AttribName == splurgeAmenityHit._source.AmenityName
                                   select x).ToList();

                if(hits.Count > 0)
                {
                    results.AddOrUpdate(splurgeAmenityHit._source.AmenityName, hits.Count, (key, oldValue) => oldValue);
                }
                }));
            }

            Task.WaitAll(tasks.ToArray());

            return results.OrderBy(x => x.Key).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        /// <summary>
        /// Gets a dictionary of destinations names and number of inventories corresponding to the supplied list of inventoryIDs.
        /// </summary>
        /// <param name="inventoryIDs">The list of inventory IDs to search by</param>
        /// <returns></returns>
        public Dictionary<string, int> GetDestinationRefinements(List<long> inventoryIDs)
        {
            GeographyQuery geographyQuery = new GeographyQuery();
            ElasticGeographySearchResult geographyResutls = geographyQuery.GetGeographyByInventoryIds(inventoryIDs);

            if (geographyResutls != null)
            {
                List<string> destinations = (from x in geographyResutls.hits.hits
                                             select x._source.Destination).ToList();

                List<string> uniqueDestinations = destinations.Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();

                ConcurrentDictionary<string, int> results = new ConcurrentDictionary<string, int>();

                foreach (string dest in uniqueDestinations)
                {
                    if (dest != null)
                    {
                        List<GeographyHit> geoHits = (from x in geographyResutls.hits.hits
                                                      where x._source.Destination == dest
                                                      select x).ToList();

                        if (geoHits.Count > 0)
                        {
                            results.AddOrUpdate(dest, geoHits.Count, (key, oldValue) => oldValue);
                        }
                    }
                }

                return results.OrderBy(x => x.Key).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            }

            return new Dictionary<string, int>();
        }

        /// <summary>
        /// Gets a dictionary of city names and number of inventories corresponding to the supplied list of inventoryIDs.
        /// </summary>
        /// <param name="inventoryIDs">The list of inventory IDs to search by</param>
        /// <returns></returns>
        public Dictionary<string, int> GetCityRefinements(List<long> inventoryIDs)
        {
            GeographyQuery geographyQuery = new GeographyQuery();
            ElasticGeographySearchResult geographyResutls = geographyQuery.GetGeographyByInventoryIds(inventoryIDs);

            List<string> cities = (from x in geographyResutls.hits.hits
                                         select x._source.CityName).ToList();

            List<string> uniqueCities = cities.Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();

            ConcurrentDictionary<string, int> results = new ConcurrentDictionary<string, int>();

            foreach (string city in uniqueCities)
            {
                if (city != null)
                {
                    List<GeographyHit> geoHits = (from x in geographyResutls.hits.hits
                                                  where x._source.Destination == city
                                                  select x).ToList();

                    if (geoHits.Count > 0)
                    {
                        results.AddOrUpdate(city, geoHits.Count, (key, oldValue) => oldValue);
                    }
                }
            }

            return results.OrderBy(x => x.Key).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        /// <summary>
        /// Gets a dictionary of state names and number of inventories corresponding to the supplied list of inventoryIDs.
        /// </summary>
        /// <param name="inventoryIDs">The list of inventory IDs to search by</param>
        /// <returns></returns>
        public Dictionary<string, int> GetStateRefinements(List<long> inventoryIDs)
        {
            GeographyQuery geographyQuery = new GeographyQuery();
            ElasticGeographySearchResult geographyResutls = geographyQuery.GetGeographyByInventoryIds(inventoryIDs);

            if (geographyResutls != null)
            {
                List<string> states = (from x in geographyResutls.hits.hits
                                       select x._source.StateName).ToList();

                List<string> uniqueStates = states.Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();

                ConcurrentDictionary<string, int> results = new ConcurrentDictionary<string, int>();

                foreach (string state in uniqueStates)
                {
                    if (state != null)
                    {
                        List<GeographyHit> geoHits = (from x in geographyResutls.hits.hits
                                                      where x._source.StateName == state
                                                      select x).ToList();

                        if (geoHits.Count > 0)
                        {
                            results.AddOrUpdate(state, geoHits.Count, (key, oldValue) => oldValue);
                        }
                    }
                }

                return results.OrderBy(x => x.Key).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            }

            return new Dictionary<string, int>();
        }

        /// <summary>
        /// Gets a dictionary of country names and number of inventories corresponding to the supplied list of inventoryIDs.
        /// </summary>
        /// <param name="inventoryIDs">The list of inventory IDs to search by</param>
        /// <returns></returns>
        public Dictionary<string, int> GetCountryRefinements(List<long> inventoryIDs)
        {
            GeographyQuery geographyQuery = new GeographyQuery();
            ElasticGeographySearchResult geographyResutls = geographyQuery.GetGeographyByInventoryIds(inventoryIDs);

            List<string> countries = (from x in geographyResutls.hits.hits
                                   select x._source.Country).ToList();

            List<string> uniqueCountries = countries.Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();

            ConcurrentDictionary<string, int> results = new ConcurrentDictionary<string, int>();

            foreach (string country in uniqueCountries)
            {
                if (country != null)
                {
                    List<GeographyHit> geoHits = (from x in geographyResutls.hits.hits
                                                  where x._source.Country == country
                                                  select x).ToList();

                    if (geoHits.Count > 0)
                    {
                        results.AddOrUpdate(country, geoHits.Count, (key, oldValue) => oldValue);
                    }
                }
            }

            return results.OrderBy(x => x.Key).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        /// <summary>
        /// Gets a dictionary of how many individuals can sleep in a unit/property and associated matching inventory counts, corresponding to the supplied list of inventoryIDs.
        /// </summary>
        /// <param name="inventoryIDs"></param>
        /// <returns></returns>
        public Dictionary<string, int> GetSleepsRefinements(List<long> inventoryIDs)
        {
            PropertyQuery propertyQuery = new PropertyQuery();
            ElasticPropertySearchResults properties = propertyQuery.GetPropertyByInventoryIDs(inventoryIDs);

            List<int> sleeps = (from x in properties.hits.hits
                                orderby x._source.Sleeps ascending
                                select x._source.Sleeps).ToList();

            sleeps = sleeps.Distinct().ToList();

            Dictionary<string, int> results = new Dictionary<string, int>();

            foreach (int sleep in sleeps)
            {
                List<PropertyHit> propHits = (from x in properties.hits.hits
                                          where x._source.Sleeps == sleep
                                          select x).ToList();

                if(propHits.Count > 0)
                {
                    results.Add(sleep.ToString(), propHits.Count);
                }
            }

            return results;
        }

        /// <summary>
        /// Gets the average rate and merchandice order and assigns them to a propertyHit
        /// </summary>
        /// <param name="hits">The property hits from an elasticsearch query</param>
        public void FillInRatesAndMerchOrder(List<PropertyHit> hits)
        {
            NightlyAverageQuery nightlyAverageQuery = new NightlyAverageQuery();
            RentalUnitContentQuery rentalUnitContentQuery = new RentalUnitContentQuery();

            List<long> inventoryIDs = (from x in hits
                                       select x._source.InventoryID).ToList();

            ElasticNightlyAverageSearchResults rateResults = nightlyAverageQuery.GetNightlyAverageRatesByInventoryIDs(inventoryIDs.ToList());
            ElasticRentalUnitContentSearchResults rentalResults = rentalUnitContentQuery.GetRentalUnitContentByInventoryIDs(inventoryIDs.ToList());

            if (rateResults != null)
            {
                foreach (NightlyAverageHit r in rateResults.hits.hits)
                {
                    hits.Select(x => x).Where(x => x._source.InventoryID == r._source.InventoryID).FirstOrDefault().rate = (double)r._source.YearAverageRate;
                }
                foreach (RentalUnitContentHit rh in rentalResults.hits.hits)
                {
                    hits.Select(x => x).Where(x => x._source.InventoryID == rh._source.InventoryId).FirstOrDefault().merchandiceOrder = rh._source.MerchandisingOrder;
                }
            }
            
        }

        /// <summary>
        /// From the supplied inventories, and the supplied amenity names, this method determines which of the supplied
        /// inventories have a corresponding amenity, if any.
        /// </summary>
        /// <param name="amenities">The amenities to search for.</param>
        /// <param name="propertyHits">The properties to be searched through, for supplied amenities</param>
        /// <returns></returns>
        public List<PropertyHit> FilterByAmenities(List<string> amenities, List<PropertyHit> propertyHits)
        {
            AmenityQuery amenityQuery = new AmenityQuery();

            List<long> inventoryIDs = (from x in propertyHits select x._source.InventoryID).ToList();

            ElasticAmenitySearchResult amenityResults = amenityQuery.GetAmenitiesByInventoryIDs(inventoryIDs);
            List<PropertyHit> filteredInventories = new List<PropertyHit>();

            foreach (string amenity in amenities)
            {
                filteredInventories.AddRange((from x in amenityResults.hits.hits
                                              join y in propertyHits
                                              on x._source.InventoryID equals y._source.InventoryID
                                              where x._source.AttribName == amenity
                                              select y).ToList());
             }

            return filteredInventories;
        }

        /// <summary>
        /// For the supplied properties, filter them by the supplied lodging type
        /// </summary>
        /// <param name="propertyHits">The properties to filter down</param>
        /// <param name="lodgingTypes">The strings of lodging types to filter the properties by</param>
        /// <returns></returns>
        public List<PropertyHit> FilterByLodingType(List<PropertyHit> propertyHits, List<string> lodgingTypes)
        {
            List<PropertyHit> filteredInventories = new List<PropertyHit>();

            foreach (string lodingType in lodgingTypes)
            {
                filteredInventories.AddRange((from x in propertyHits
                                              where x._source.BuildingType.ToLower() == lodingType.ToLower()
                                              select x).ToList());
            }

            return filteredInventories;
        }

        /// <summary>
        /// From the supplied inventories, and the supplied sleeps array, this method determins which of the supplied 
        /// inventories have a corresponding sleeps 
        /// </summary>
        /// <param name="sleeps"></param>
        /// <param name="propertyHits"></param>
        /// <param name="isLuxury"></param>
        /// <returns></returns>
        public List<PropertyHit> FilterBySleeps(List<int> sleeps, List<PropertyHit> propertyHits, bool? isLuxury=null)
        {
            //PropertyQuery propertyQuery = new PropertyQuery();
            //List<PropertyHit> propertySources = new List<PropertyHit>();

            //foreach (int b in sleeps)
            //{
            //    ElasticPropertySearchResults propertyResults = null;

            //    if (isLuxury == null)
            //    {
            //        propertyResults = propertyQuery.GetLuxuryPropertiesbyBed(b.ToString());
            //        propertyResults = propertyQuery.GetPropertiesbyBed(b.ToString());
            //    }
            //    else if ((bool)isLuxury)
            //        propertyResults = propertyQuery.GetLuxuryPropertiesbyBed(b.ToString());
            //    else
            //        propertyResults = propertyQuery.GetPropertiesbyBed(b.ToString());

            //    propertySources.AddRange(propertyResults.hits.hits);

            //    if (propertyResults.hits.hits != null && propertyResults.hits.hits.Length > 0)
            //    {
            //        propertySources.AddRange(propertyResults.hits.hits);
            //    }
            //}

            List<PropertyHit> filteredInventories = new List<PropertyHit>();

            foreach (int sleep in sleeps)
            {
                filteredInventories.AddRange((from x in propertyHits
                                              where x._source.Sleeps == sleep
                                              select x).ToList());
             }

            return filteredInventories;
        }

        /// <summary>
        /// For the supplied inventories, and the supplied bedrooms array, this method determins which of the supplied
        /// inventories have a corresponding bedroom count
        /// </summary>
        /// <param name="bedrooms"></param>
        /// <param name="propertyHits"></param>
        /// <returns></returns>
        public List<PropertyHit> FilterByBedrooms(List<int> bedrooms, List<PropertyHit> propertyHits)
        {
            List<PropertyHit> filteredInventories = new List<PropertyHit>();

            foreach (int numBed in bedrooms)
            {
                filteredInventories.AddRange((from x in propertyHits
                                              where x._source.Bedroom == numBed
                                              select x).ToList());
            }

            return filteredInventories;
        }

        public List<PropertyHit> FilterByViewType(List<string> views, List<PropertyHit> propertyHits)
        {
            List<PropertyHit> filteredInventories = new List<PropertyHit>();

            foreach (string v in views)
            {
                filteredInventories.AddRange((from x in propertyHits
                                              where (x._source.PropertyView == null ? "" : x._source.PropertyView.ToLower())  == v
                                              select x).ToList());
            }

            return filteredInventories;
        }

        /// <summary>
        /// Returns a list of PropertyInfos, based on supplied inventoryIDs
        /// </summary>
        /// <param name="inventoryIDs">The list of inventoryIDs used to populate a list of PropertyInfos</param>
        /// <returns></returns>
        public List<PropertyInfo> FillPropertyInfos(List<long> inventoryIDs)
        {
            PropertyQuery propertyQuery = new PropertyQuery();
            RentalUnitContentQuery rentalUnitContentQuery = new RentalUnitContentQuery();
            NightlyAverageQuery nightlyAverageQuery = new NightlyAverageQuery();
            GeographyQuery geographyQuery = new GeographyQuery();
            
            List<PropertyInfo> infos = new List<PropertyInfo>();

            foreach (long inventoryID in inventoryIDs)
            {
                ElasticPropertySearchResults propertyResults = propertyQuery.GetPropertyByInventoryID(inventoryID);
                ElasticRentalUnitContentSearchResults rentalUnitResults = rentalUnitContentQuery.GetRentalUnitContentByInventoryID(inventoryID);
                ElasticNightlyAverageSearchResults nightlyAverage = nightlyAverageQuery.GetNightlyAverageRateByInventoryID(inventoryID);
                

                if (propertyResults.hits.hits.Count() == 1 && rentalUnitResults.hits.hits.Count() == 1)
                {
                    PropertyHit property = propertyResults.hits.hits[0];
                    RentalUnitContentHit rentalUnit = rentalUnitResults.hits.hits[0];

                    ElasticGeographySearchResult elasticGeographySearchResult = geographyQuery.GetGeographyByInventoryId(rentalUnit._source.InventoryId);
                    string destination = "";
                    
                    if(elasticGeographySearchResult.hits != null)
                    {
                        if(elasticGeographySearchResult.hits.hits != null)
                        {
                            if(elasticGeographySearchResult.hits.hits.Length > 0)
                            {
                                destination = elasticGeographySearchResult.hits.hits[0]._source.Destination;
                            }
                        }
                    }

                    decimal price = 0;

                    if (nightlyAverage.hits.hits.Count() >= 1)
                    {
                        price = nightlyAverage.hits.hits[0]._source.YearAverageRate;
                    }

                    PropertyInfo propertyInfo = new PropertyInfo()
                    {
                        Destination = destination,
                        BedRooms = property._source.Bedroom.ToString(),
                        CheckInTime = rentalUnit._source.CheckInTime,
                        CheckOutTime = rentalUnit._source.CheckOutTime,
                        ComplexDescription = rentalUnit._source.ComplexDescription,
                        UnitDescription = rentalUnit._source.UnitDescription,
                        ComplexName = rentalUnit._source.ComplexName,
                        Disclaimer = rentalUnit._source.Disclaimer,
                        InventoryID = rentalUnit._source.InventoryId,
                        RentalUnitID = rentalUnit._source.RentalUnitId,
                        IsFeatured = property._source.IsFeaturedProperty,
                        IsLuxury = property._source.IsLuxury,
                        IsBookable = rentalUnit._source.IsAvailableForBooking,
                        Latitude = rentalUnit._source.Latitude,
                        Longitude = rentalUnit._source.Longitude,
                        Occupancy = rentalUnit._source.Occupancy == null ? 0 : (int)rentalUnit._source.Occupancy,
                        Pricing = price.ToString("0.00"),
                        Sleeps = property._source.Sleeps.ToString(),
                        Baths = rentalUnit._source.FullBathrooms.ToString(),
                        Policies = property._source.Policies,
                        Type = property._source.BuildingType,
                        UnitNumber = rentalUnit._source.UnitNumber,
                        Images = GetImages(property._source.InventoryID),
                        ViewType = property._source.PropertyView
                    };

                    infos.Add(propertyInfo);
                }
            }

            return infos;
        }

        /// <summary>
        /// Decrypts a message
        /// </summary>
        /// <param name="Message">The message to decrypt</param>
        /// <param name="Passphrase">The phrase to decrypt with</param>
        /// <returns>The decrypted message</returns>
        public string Decrypt(string Message, string Passphrase)
        {
            try
            {
                byte[] Results;
                System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

                MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
                byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

                TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

                TDESAlgorithm.Key = TDESKey;
                TDESAlgorithm.Mode = CipherMode.ECB;
                TDESAlgorithm.Padding = PaddingMode.PKCS7;

                byte[] DataToDecrypt = Convert.FromBase64String(Message);

                try
                {
                    ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                    Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
                }
                finally
                {
                    TDESAlgorithm.Clear();
                    HashProvider.Clear();
                }

                return UTF8.GetString(Results);
            }
            catch(Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// Encrypt a message with a passphrase
        /// </summary>
        /// <param name="Message">The message to encrypt</param>
        /// <param name="Passphrase">The passphrase to encrypt with</param>
        /// <returns>The encrypted message</returns>
        public string Encrypt(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;

            byte[] DataToEncrypt = UTF8.GetBytes(Message);

            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            return Convert.ToBase64String(Results);
        }

        /// <summary>
        /// Gets a list of images for a given inventory ID
        /// </summary>
        /// <param name="inventoryID">The inventory ID of the property</param>
        /// <returns></returns>
        public List<string> GetImages(long inventoryID)
        {
            ImageQuery imageQuery = new ImageQuery();
            ElasticImageSearchResults imageResults = imageQuery.GetImagesForProperty(inventoryID);

            if(imageResults.hits != null)
            {
                if(imageResults.hits.hits.Count() == 1)
                {
                    return imageResults.hits.hits[0]._source.Images;
                }
            }

            return new List<string>();
        }
    }
}
﻿using Elasticsearch.Net;
using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Amenities;
using ElasticSearchCore.API.Connector;
using ElasticSearchCore.Models.Insert;
using ElasticSearchCore.Models.Responses;
using ElasticSearchCore.Models.Responses.Amenities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class AmenityQuery
    {
        //private ElasticSearchConnection elasticSearchConnection = null;
        //private string path = "props/amenities/_search";

        ElasticLowLevelClient lowLevelClient = null;

        public AmenityQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        /// <summary>
        /// Gets the amenities for given a given inventory ID
        /// </summary>
        /// <param name="inventoryID">a list of inventory IDs</param>
        /// <returns>ElasticAmenitySearchResul object</returns>
        public ElasticAmenitySearchResult GetAmenitiesByInventoryID(long inventoryID)
        {
            //form the query to send to ElasticSearch
            string query = "{" +
                                "\"size\": 10000," +
                                  "\"query\" : {" +
                                    "\"term\": { " +
                                        "\"InventoryID\": " + inventoryID +
                                        "}" +
                                    "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("amenities", "amenity", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticAmenitySearchResult resultAmenities = GetResponse<ElasticAmenitySearchResult>(responseJson);

                return resultAmenities;
            }

            return default(ElasticAmenitySearchResult);
        }

        /// <summary>
        /// Gets the amenities for given inventory IDs
        /// </summary>
        /// <param name="inventoryIDs">a list of inventory IDs</param>
        /// <returns>ElasticAmenitySearchResul object</returns>
        public ElasticAmenitySearchResult GetAmenitiesByInventoryIDs(List<long> inventoryIDs)
        {
            string idStr = "";

            for(int i=0; i < inventoryIDs.Count; i++)
            {
                idStr += inventoryIDs[i].ToString();

                if (i < inventoryIDs.Count - 1)
                    idStr += ",";
            }

            //form the query to send to ElasticSearch
            string query = "{" +
                                "\"size\": 10000," +
                                  "\"query\" : {" +
                                    "\"terms\": { " +
                                        "\"InventoryID\": [" + idStr + "]"+
                                        "}" +
                                    "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("amenities", "amenity", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticAmenitySearchResult resultAmenities = GetResponse<ElasticAmenitySearchResult>(responseJson);

                return resultAmenities;
            }

            return default(ElasticAmenitySearchResult);
        }

        /// <summary>
        /// Gets and returns all amenities from elasticsearch
        /// </summary>
        /// <returns></returns>
        public List<AmenityHit> GetAllAmenities()
        {
            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            StringResponse response = lowLevelClient.SearchGet<StringResponse>("amenities", "amenity", searchRequestParameters);

            List<AmenityHit> hits = new List<AmenityHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticAmenitySearchResult props = GetResponse<ElasticAmenitySearchResult>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticAmenitySearchResult results = GetScrollResults<ElasticAmenitySearchResult>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            return hits;
        }

        /// <summary>
        /// Deletes an amenity from Elasticsearch
        /// </summary>
        /// <param name="id">the elasticsearch id of the amenity</param>
        /// <returns></returns>
        public ElasticDeleteResults DeleteAmenity(string id)
        {
            var searchResponse = lowLevelClient.Delete<StringResponse>("amenities", "amenity", id);
            var successful = searchResponse.Success;
            var responseJson = searchResponse.Body;

            ElasticDeleteResults result = GetResponse<ElasticDeleteResults>(responseJson);

            return result;
        }

        /// <summary>
        /// Deletes a splurgeAmenity from Elasticsearch
        /// </summary>
        /// <param name="id">the elasticsearch id of the splurgeAmenity</param>
        /// <returns></returns>
        public ElasticDeleteResults DeleteSplurgeAmenity(string id)
        {
            var searchResponse = lowLevelClient.Delete<StringResponse>("splurgeamenities", "splurgeAmenity", id);
            var successful = searchResponse.Success;
            var responseJson = searchResponse.Body;

            ElasticDeleteResults result = GetResponse<ElasticDeleteResults>(responseJson);

            return result;
        }

        /// <summary>
        /// Gets and returns all splurge amenities from Elasticsearch
        /// </summary>
        /// <returns></returns>
        public List<SplurgeAmenityHit> GetSplurgeAmenities()
        {
            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            List<SplurgeAmenityHit> hits = new List<SplurgeAmenityHit>();

            StringResponse response = lowLevelClient.SearchGet<StringResponse>("splurgeamenities", "splurgeAmenity", searchRequestParameters);

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticSplurgeAmenitySearchResult props = GetResponse<ElasticSplurgeAmenitySearchResult>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticSplurgeAmenitySearchResult results = GetScrollResults<ElasticSplurgeAmenitySearchResult>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }

                return hits;
            }

            return new List<SplurgeAmenityHit>();
        }
    }
}
﻿using Elasticsearch.Net;
using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.WeeklyRates;
using ElasticSearchCore.API.Connector;
using ElasticSearchCore.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class WeeklyRateQuery
    {
        ElasticLowLevelClient lowLevelClient = null;

        public WeeklyRateQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        /// <summary>
        /// Gets the weeklyRates for a given property
        /// </summary>
        /// <param name="inventoryIDs">the inventoryID IDs</param>
        /// <returns>ElasticWeeklyRateSearchResult object</returns>
        public ElasticWeeklyRateSearchResult GetWeeklyRateByInventoryID(long inventoryID)
        {
            //form the query to send to ElasticSearch
            string query = "{" +
                                "\"size\": 10000," +
                                  "\"query\" : {" +
                                    "\"term\": { " +
                                        "\"InventoryID\": " + inventoryID +
                                        "}" +
                                    "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("weeklyrates", "weeklyRate", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticWeeklyRateSearchResult resultProperties = GetResponse<ElasticWeeklyRateSearchResult>(responseJson);

                return resultProperties;
            }

            return default(ElasticWeeklyRateSearchResult);
        }

        /// <summary>
        /// Gets the weeklyRates for a given inventory Ids
        /// </summary>
        /// <param name="inventoryIDs">the inventoryID IDs</param>
        /// <returns>ElasticWeeklyRateSearchResult object</returns>
        public ElasticWeeklyRateSearchResult GetWeeklyRateByInventoryIDs(List<long> inventoryIDs)
        {
            string idStr = "";

            for (int i = 0; i < inventoryIDs.Count; i++)
            {
                idStr += inventoryIDs[i].ToString();

                if (i < inventoryIDs.Count - 1)
                    idStr += ",";
            }

            //form the query to send to ElasticSearch
            string query = "{" +
                                "\"size\": 10000," +
                                  "\"query\" : {" +
                                    "\"terms\": { " +
                                        "\"InventoryID\": [" + idStr + "]" +
                                        "}" +
                                    "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("weeklyrates", "weeklyRate", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticWeeklyRateSearchResult resultProperties = GetResponse<ElasticWeeklyRateSearchResult>(responseJson);

                return resultProperties;
            }

            return default(ElasticWeeklyRateSearchResult);
        }

        /// <summary>
        /// Gets all weekly rates from elastic search
        /// </summary>
        /// <returns></returns>
        public List<WeeklyRateHit> GetAllWeeklyRates()
        {
            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            StringResponse response = lowLevelClient.SearchGet<StringResponse>("weeklyrates", "weeklyRate", searchRequestParameters);

            List<WeeklyRateHit> hits = new List<WeeklyRateHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticWeeklyRateSearchResult props = GetResponse<ElasticWeeklyRateSearchResult>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticWeeklyRateSearchResult results = GetScrollResults<ElasticWeeklyRateSearchResult>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            return hits;
        }

        /// <summary>
        /// Delete a weeklly rate from elasticsearch
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ElasticDeleteResults DeleteWeeklyRate(string id)
        {
            var searchResponse = lowLevelClient.Delete<StringResponse>("weeklyrates", "weeklyRate", id);
            var successful = searchResponse.Success;
            var responseJson = searchResponse.Body;

            ElasticDeleteResults result = GetResponse<ElasticDeleteResults>(responseJson);

            return result;
        }
    }
}

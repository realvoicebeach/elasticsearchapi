﻿using Elasticsearch.Net;
using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Neighborhoods;
using ElasticSearchCore.API.Connector;
using ElasticSearchCore.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class NeighborhoodQuery
    {
        ElasticLowLevelClient lowLevelClient = null;

        public NeighborhoodQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        /// <summary>
        /// Gets the neighborhood by inventoryID
        /// </summary>
        /// <param name="inventoryId">the selected inventoryID to use</param>
        /// <returns>lasticNeighborhoodSearchResult obj</returns>
        public ElasticNeighborhoodSearchResult GetNeighborhoodByInventoryID(string inventoryId)
        {
            string query = "{" +
                              "\"query\" : { " +
                                    "\"term\" : {" +
                                      "\"InventoryID\":" + inventoryId +
                                    "}" +

                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("neighborhoods", "neighborhood", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticNeighborhoodSearchResult results = GetResponse<ElasticNeighborhoodSearchResult>(responseJson);

                return results;
            }

            return default(ElasticNeighborhoodSearchResult);
        }

        /// <summary>
        /// Gets the neighborhoods by inventoryIDs
        /// </summary>
        /// <param name="inventoryIDs">the selected inventory IDs</param>
        /// <returns>ElasticNeighborhoodSearchResult</returns>
        public ElasticNeighborhoodSearchResult GetNeighborhoodByInventoryIDs(List<long> inventoryIDs)
        {
            string idStr = "";

            for (int i = 0; i < inventoryIDs.Count; i++)
            {
                idStr += inventoryIDs[i].ToString();

                if (i < inventoryIDs.Count - 1)
                    idStr += ",";
            }

            string query = "{" +
                              "\"size\":10000," +
                              "\"query\" : { " +
                                    "\"terms\" : {" +
                                      "\"InventoryID\":  [" + idStr + "]" +
                                    "}" +

                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("neighborhoods", "neighborhood", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticNeighborhoodSearchResult results = GetResponse<ElasticNeighborhoodSearchResult>(responseJson);

                return results;
            }

            return default(ElasticNeighborhoodSearchResult);
        }
        
        /// <summary>
        /// Gets all neighborhoods from elasticsearch
        /// </summary>
        /// <returns></returns>
        public List<NeighborhoodHit> GetAllNeighborhoods()
        {
            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            StringResponse response = lowLevelClient.SearchGet<StringResponse>("neighborhoods", "neighborhood", searchRequestParameters);

            List<NeighborhoodHit> hits = new List<NeighborhoodHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticNeighborhoodSearchResult props = GetResponse<ElasticNeighborhoodSearchResult>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticNeighborhoodSearchResult results = GetScrollResults<ElasticNeighborhoodSearchResult>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            return hits;
        }

        /// <summary>
        /// Remove a neighborhood document from elasticsearch
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ElasticDeleteResults DeleteNeighborhood(string id)
        {
            var searchResponse = lowLevelClient.Delete<StringResponse>("neighborhoods", "neighborhood", id);
            var successful = searchResponse.Success;
            var responseJson = searchResponse.Body;

            ElasticDeleteResults result = GetResponse<ElasticDeleteResults>(responseJson);

            return result;
        }
    }
}

﻿using Elasticsearch.Net;
using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.Geography;
using ElasticSearchCore.API.Connector;
using ElasticSearchCore.Core.Domain;
using ElasticSearchCore.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class GeographyQuery
    {
        ElasticLowLevelClient lowLevelClient = null;

        public GeographyQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        /// <summary>
        /// Gets the geography associated with an inventory ID
        /// </summary>
        /// <param name="inventoryId">inventory ID</param>
        /// <returns>ElasticGeographySearchResult object</returns>
        public ElasticGeographySearchResult GetGeographyByInventoryId(long inventoryId)
        {
            string query = "{" +
                              "\"query\" : { " +
                                    "\"term\" : {" +
                                      "\"InventoryID\":" + inventoryId +
                                    "}" +

                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("geographies", "geography", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticGeographySearchResult results = GetResponse<ElasticGeographySearchResult>(responseJson);

                return results;
            }

            return default(ElasticGeographySearchResult);
        }

        /// <summary>
        /// Gets the geography associated with an inventory ID
        /// </summary>
        /// <param name="inventoryId">inventory IDs</param>
        /// <returns>ElasticGeographySearchResult object</returns>
        public ElasticGeographySearchResult GetGeographyByInventoryIds(List<long> inventoryIDs)
        {
            string idStr = "";

            for (int i = 0; i < inventoryIDs.Count; i++)
            {
                idStr += inventoryIDs[i].ToString();

                if (i < inventoryIDs.Count - 1)
                    idStr += ",";
            }

            string query = "{" +
                              "\"size\":10000," +
                              "\"query\" : { " +
                                    "\"terms\" : {" +
                                      "\"InventoryID\": [" + idStr + "]"+
                                    "}" +

                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("geographies", "geography", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticGeographySearchResult results = GetResponse<ElasticGeographySearchResult>(responseJson);

                return results;
            }

            return default(ElasticGeographySearchResult);
        }

        /// <summary>
        /// Deletes a Geography from ElasticSearch
        /// </summary>
        /// <param name="id">the ElasticSearch "_id" of this entry</param>
        /// <returns></returns>
        public ElasticDeleteResults DeleteGeographyById(string id)
        {
            var searchResponse = lowLevelClient.Delete<StringResponse>("geographies", "geography", id);
            var successful = searchResponse.Success;
            var responseJson = searchResponse.Body;

            ElasticDeleteResults result = GetResponse<ElasticDeleteResults>(responseJson);

            return result;
        }

        /// <summary>
        /// Indexes a new object into ElasticSearch
        /// </summary>
        /// <param name="geography">The Geography to index</param>
        /// <param name="parent_id">the "_id" of the property parent</param>
        /// <returns></returns>
        [Obsolete("Method is now obsolete.")]
        public ElasticCreateResult InsertNewGeography(EndecaGeographyView geography, string parent_id)
        {
            throw new NotImplementedException();

            //return elasticSearchConnection.Insert<EndecaGeographyView>(createPath + parent_id, geography);
        }

        /// <summary>
        /// Gets all geographies from elasticsearch
        /// </summary>
        /// <returns></returns>
        public List<GeographyHit> GetAllGeographies()
        {
            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            StringResponse response = lowLevelClient.SearchGet<StringResponse>("geographies", "geography", searchRequestParameters);

            List<GeographyHit> hits = new List<GeographyHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticGeographySearchResult props = GetResponse<ElasticGeographySearchResult>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticGeographySearchResult results = GetScrollResults<ElasticGeographySearchResult>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            return hits;
        }


        public ElasticGeographySearchResult GetGeographiesByDestination(string destination)
        {
            string query = "{" +
                              "\"query\" : { " +
                                    "\"match\" : {" +
                                      "\"Destination\": \""+ destination +"\""+
                                    "}" +

                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("geographies", "geography", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticGeographySearchResult results = GetResponse<ElasticGeographySearchResult>(responseJson);

                return results;
            }

            return default(ElasticGeographySearchResult);
        }

    }
}
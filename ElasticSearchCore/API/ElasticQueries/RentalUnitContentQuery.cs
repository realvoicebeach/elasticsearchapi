﻿using Elasticsearch.Net;
using ElasticSearch.Models.Responses;
using ElasticSearch.Models.Responses.RentalUnitContent;
using ElasticSearchCore.API.Connector;
using ElasticSearchCore.Core.Domain;
using ElasticSearchCore.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Text;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class RentalUnitContentQuery
    {
        ElasticLowLevelClient lowLevelClient = null;

        public RentalUnitContentQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        /// <summary>
        /// Gets the RentalUnitContent by inventory ID
        /// </summary>
        /// <param name="inventoryId">the inventory ID</param>
        /// <returns>ElasticRentalUnitContentSearchResults object</returns>
        public ElasticRentalUnitContentSearchResults GetRentalUnitContentByInventoryID(long inventoryId)
        {
            string query = "{" +
                              "\"query\": {" +
                                    "\"term\": {" +
                                        "\"InventoryId\": {" +
                                            "\"value\": " + inventoryId +
                                        "}" +
                                    "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("rentalunitcontents", "rentalUnitContent", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticRentalUnitContentSearchResults results = GetResponse<ElasticRentalUnitContentSearchResults>(responseJson);

                return results;
            }

            return default(ElasticRentalUnitContentSearchResults);
        }

        /// <summary>
        /// Gets the RentalUnitContent by inventory IDs
        /// </summary>
        /// <param name="inventoryId">the inventory IDs</param>
        /// <returns>ElasticRentalUnitContentSearchResults object</returns>
        public ElasticRentalUnitContentSearchResults GetRentalUnitContentByInventoryIDs(List<long> inventoryIDs)
        {
            string idStr = "";

            for (int i = 0; i < inventoryIDs.Count; i++)
            {
                idStr += inventoryIDs[i].ToString();

                if (i < inventoryIDs.Count - 1)
                    idStr += ",";
            }

            string query = "{" +
                              "\"size\":10000," +
                              "\"query\": {" +
                                    "\"terms\": {" +
                                       "\"InventoryId\":  [" + idStr + "]" +
                                    "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("rentalunitcontents", "rentalUnitContent", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticRentalUnitContentSearchResults results = GetResponse<ElasticRentalUnitContentSearchResults>(responseJson);

                return results;
            }

            return default(ElasticRentalUnitContentSearchResults);
        }

        /// <summary>
        /// Deletes a RentalUnitContent from ElasticSearch
        /// </summary>
        /// <param name="id">the ElasticSearch "_id" of this entry</param>
        /// <returns></returns>
        public ElasticDeleteResults DeleteRentalUnitContentById(string id)
        {
            var searchResponse = lowLevelClient.Delete<StringResponse>("rentalunitcontents", "rentalUnitContent", id);
            var successful = searchResponse.Success;
            var responseJson = searchResponse.Body;

            ElasticDeleteResults result = GetResponse<ElasticDeleteResults>(responseJson);

            return result;
        }

        /// <summary>
        /// Indexes a new object into ElasticSearch
        /// </summary>
        /// <param name="rental">The rentalUnitContent to index</param>
        /// <param name="parent_id">the "_id" of the property parent</param>
        /// <returns></returns>
        [Obsolete("Method is now obsolete.")]
        public ElasticCreateResult InsertNewRentalUnitContent(RentalUnitContentView rental, string parent_id)
        {
            throw new NotImplementedException();

            //return elasticSearchConnection.Insert<RentalUnitContentView>(createPath + parent_id, rental);
        }

        /// <summary>
        /// Gets all rentalUnitContents from elasticsearch
        /// </summary>
        /// <returns></returns>
        public List<RentalUnitContentHit> GetAllRentalUnitContents()
        {
            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            StringResponse response = lowLevelClient.SearchGet<StringResponse>("rentalunitcontents", "rentalUnitContent", searchRequestParameters);

            List<RentalUnitContentHit> hits = new List<RentalUnitContentHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticRentalUnitContentSearchResults props = GetResponse<ElasticRentalUnitContentSearchResults>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticRentalUnitContentSearchResults results = GetScrollResults<ElasticRentalUnitContentSearchResults>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            return hits;
        }
    }
}
﻿using Elasticsearch.Net;
using ElasticSearchCore.Models.Responses;
using ElasticSearchCore.Models.Responses.BusinessUnits;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class BusinessUnitQuery
    {
        ElasticLowLevelClient lowLevelClient = null;

        public BusinessUnitQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        /// <summary>
        /// Checks to see if a BusinessUnit exists in Elasticsearch
        /// </summary>
        /// <param name="businessUnitName">The business unit name</param>
        /// <param name="inventoryId">The associated inventory ID</param>
        /// <param name="rentalUnitId">The associated rental Unit ID</param>
        /// <returns>True/False</returns>
        public bool BusinessUnitExists(string businessUnitName, long inventoryId, long rentalUnitId)
        {
            string query = "{" +
                              "\"query\": {" +
                                "\"bool\": {" +
                                    "\"must\": [" +
                                     "{" +
                                        "\"match_phrase\": {" +
                                        "\"BusinessUnitName\": \"" + businessUnitName + "\"" +
                                        "}" +
                                    "}," +
                                    "{" +
                                        "\"match\": {" +
                                        "\"InventoryId\":" + inventoryId +
                                        "}" +
                                    "}," +
                                    "{" +
                                        "\"match\": {" +
                                        "\"RentalUnitId\": " + rentalUnitId +
                                        "}" +
                                    "}" +
                                    "]" +
                                "}" +
                                "}" +
                            "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("businessunits", "businessUnit", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticBusinessUnitSearchResult results = GetResponse<ElasticBusinessUnitSearchResult>(responseJson);

                if (results.hits.hits.Count() >= 1)
                    return true;
                else
                    return false;
            }

            return false;
        }

        /// <summary>
        /// Gets all BusinessUnits by a specific name
        /// </summary>
        /// <param name="businessUnitName">The name of the Business Unit</param>
        /// <returns>List of Business Unit Hits</returns>
        public List<BusinessUnitHit> GetAllBusinessUnitsByBusinessUnitName(string businessUnitName)
        {
            string query = "{" +
                              "\"query\": {" +
                                "\"bool\": {" +
                                    "\"must\": [" +
                                     "{" +
                                        "\"match_phrase\": {" +
                                        "\"BusinessUnitName\": \"" + businessUnitName + "\"" +
                                        "}" +
                                     "}" +
                                    "]" +
                                  "}" +
                                "}" +
                            "}";

            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            StringResponse response = lowLevelClient.Search<StringResponse>("businessunits", "businessUnit", query, searchRequestParameters);

            List<BusinessUnitHit> hits = new List<BusinessUnitHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticBusinessUnitSearchResult props = GetResponse<ElasticBusinessUnitSearchResult>(responseJson);

                if (props.hits.hits.Length != 0)
                    hits.AddRange(props.hits.hits);

                string scrollID = props._scroll_id;

                while (true)
                {
                    ElasticBusinessUnitSearchResult results = GetScrollResults<ElasticBusinessUnitSearchResult>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    hits.AddRange(results.hits.hits);
                }
            }

            return hits;        
        }

        /// <summary>
        /// Returns a list of Business Unit Names for a given inventory ID
        /// </summary>
        /// <param name="inventoryID">The inventory ID to use</param>
        /// <returns></returns>
        public List<String> GetBusinessUnitsForInventoryID(string inventoryID)
        {
            List<string> businessUnitNames = new List<string>();

            string query = "{"+
                               "\"size: 100, "+
                              "\"query\": {"+
                                            "\"match\":{"+
                                                "\"InventoryId\": " + inventoryID +
                                            "}" +
                                        "}" +
                                    "}";

            var searchResponse = lowLevelClient.Search<StringResponse>("businessunits", "businessUnit", query);
            var successful = searchResponse.Success;

            if (successful)
            {
                var responseJson = searchResponse.Body;

                ElasticBusinessUnitSearchResult results = GetResponse<ElasticBusinessUnitSearchResult>(responseJson);
                
                if(results.hits != null)
                {
                    foreach(BusinessUnitHit hit in results.hits.hits)
                    {
                        businessUnitNames.Add(hit._source.BusinessUnitName);
                    }
                }
            }

            return businessUnitNames;
        }

        /// <summary>
        /// Deletes a BusinessUnit from ElasticSearch
        /// </summary>
        /// <param name="id">the ElasticSearch "_id" of this entry</param>
        /// <returns></returns>
        public ElasticDeleteResults DeleteBusinessUnitById(string id)
        {
            var searchResponse = lowLevelClient.Delete<StringResponse>("businessunits", "businessUnit", id);
            var successful = searchResponse.Success;
            var responseJson = searchResponse.Body;

            ElasticDeleteResults result = GetResponse<ElasticDeleteResults>(responseJson);

            return result;
        }
    }
}

﻿using Elasticsearch.Net;
using ElasticSearch.Models.Responses.PluginKeyDomain;
using ElasticSearchCore.Models.Insert;
using ElasticSearchCore.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class DomainAndKeyQuery
    {
        ElasticLowLevelClient lowLevelClient = null;

        public DomainAndKeyQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        private List<PluginKeyDomainHit> GetDomainEntryByActivationKey(string activationKey)
        {
            string query = "{" +
                                "\"query\": {" +
                                    "\"match\": {" +
                                        "\"PluginActivationKey\": \""+ activationKey +"\""+
                                    "}" +
                                "}" +
                           "}";

            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            var response = lowLevelClient.Search<StringResponse>("pluginkeydomain", "pkd", query, searchRequestParameters);
            var successful = response.Success;

            List<PluginKeyDomainHit> hits = new List<PluginKeyDomainHit>();

            if (successful)
            {
                var responseJson = response.Body;

                ElasticPluginKeyDomainSearchResults results = GetResponse<ElasticPluginKeyDomainSearchResults>(responseJson);

                if (results.hits.hits.Length != 0)
                {
                    hits.AddRange(results.hits.hits);
                }

                string scrollID = results._scroll_id;

                while (true)
                {
                    results = GetScrollResults<ElasticPluginKeyDomainSearchResults>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    foreach (PluginKeyDomainHit x in results.hits.hits)
                    {
                        hits.AddRange(results.hits.hits);
                    }
                }
            }

            return hits;
        }

        private int UpdateKeyAndDomain(string id, string pluginKey, string domain, string pluginActivationKey)
        {
            string query = "{" +
                                "\"PluginKey\": \"" + pluginKey + "\"," +
                                "\"PluginDomain\": \"" + domain + "\"," +
                                "\"PluginActivationKey\": \"" + pluginActivationKey + "\"" +
                            "}";

            StringResponse response = lowLevelClient.IndexPut<StringResponse>("pluginkeydomain","pkd", id, PostData.String(query));

            if(response.Success)
            {
                return 1;
            }

            return 0;
        }

        /// <summary>
        /// If a valid PluginActivationKey is used, insert the domain and plugin key into the elasticsearch
        /// </summary>
        /// <param name="pkd">the ElasticPluginKeyDomain object</param>
        /// <returns></returns>
        public int SaveKeyWithDomain(ElasticPluginKeyDomain pkd)
        {
            try
            {
                List<PluginKeyDomainHit> elasticPluginKeyDomains = GetDomainEntryByActivationKey(pkd.PluginActivationKey);

                if (elasticPluginKeyDomains.Count > 0)
                {
                    PluginKeyDomainHit epkd = elasticPluginKeyDomains.First();

                    return UpdateKeyAndDomain(epkd._id, pkd.PluginKey, pkd.PluginDomain, epkd._source.PluginActivationKey);
                }
                else
                {
                    return 0;
                }
            }
            catch(Exception)
            {

            }

            return -1;
        }

        /// <summary>
        /// Gets a list of all Plugin keys and domains
        /// </summary>
        /// <returns></returns>
        public List<ElasticPluginKeyDomain> GetAllPluginKeysAndDomains()
        {
            string query = "{" +
                                "\"query\": {" +
                                    "\"match_all\": { }" +
                                "}" +
                           "}";

            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            var response = lowLevelClient.Search<StringResponse>("pluginkeydomain", "pkd", query, searchRequestParameters);
            var successful = response.Success;

            List<ElasticPluginKeyDomain> hits = new List<ElasticPluginKeyDomain>();

            if (successful)
            {
                var responseJson = response.Body;

                ElasticPluginKeyDomainSearchResults results = GetResponse<ElasticPluginKeyDomainSearchResults>(responseJson);

                if (results.hits.hits.Length != 0)
                {
                    foreach (PluginKeyDomainHit x in results.hits.hits)
                    {
                        hits.Add(x._source);
                    }
                }

                string scrollID = results._scroll_id;

                while (true)
                {
                    results = GetScrollResults<ElasticPluginKeyDomainSearchResults>(scrollID);

                    if (results.hits.hits.Length == 0)
                        break;

                    scrollID = results._scroll_id;

                    foreach (PluginKeyDomainHit x in results.hits.hits)
                    {
                        hits.Add(x._source);
                    }
                }
            }

            return hits;
        }
    }
}

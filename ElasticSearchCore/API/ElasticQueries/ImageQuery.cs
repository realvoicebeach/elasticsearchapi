﻿using Elasticsearch.Net;
using ElasticSearchCore.Models.Responses;
using ElasticSearchCore.Models.Responses.Images;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.API.ElasticQueries
{
    public class ImageQuery
    {
        ElasticLowLevelClient lowLevelClient = null;

        public ImageQuery()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["ElasticSearchPort"]);

            var settings = new ConnectionConfiguration(new Uri($"{host}:{port}")).RequestTimeout(TimeSpan.FromMinutes(2));
            lowLevelClient = new ElasticLowLevelClient(settings);
        }

        private T GetResponse<T>(string content)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            T result = JsonConvert.DeserializeObject<T>(content, settings);

            return result;
        }

        private T GetScrollResults<T>(string scrollID)
        {
            string query = "{" +
                            "\"scroll\" : \"1m\", " +
                            "\"scroll_id\" : \"" + scrollID + "\"" +
                            "}";

            StringResponse response = lowLevelClient.Scroll<StringResponse>(PostData.String(query), null);

            if (response.Success)
            {
                var results = GetResponse<T>(response.Body);
                return results;
            }

            return default(T);
        }

        public ElasticImageSearchResults GetImagesForProperty(long inventoryID)
        {
            string query = "{"+
                              "\"query\": {" +
                                    "\"match\": {" +
                                        "\"InventoryID\": \""+ inventoryID +"\"" +
                                    "}"+
                                "}"+
                            "}";

            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.Scroll = TimeSpan.FromMinutes(1);

            var response = lowLevelClient.Search<StringResponse>("propertyimages", "images", query, searchRequestParameters);

            List<ImageHit> hits = new List<ImageHit>();

            if (response.Success)
            {
                var responseJson = response.Body;

                ElasticImageSearchResults images = GetResponse<ElasticImageSearchResults>(responseJson);

                return images;
            }

            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class EndecaPropertyViewRepository : IEndecaPropertyViewRepository
    {
        private Connection _connection;

        public EndecaPropertyViewRepository()
        {
            _connection = new Connection();
        }

        public List<EndecaPropertyView> GetAllProperties()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPropertyViews
                        select x).ToList();
            }
        }

        public List<long> GetInventoryIDsByBeds(List<string> bedRooms)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> inventoryIDs = new List<long>();

                foreach (string bedRoom in bedRooms)
                {
                    string br = bedRoom.Replace("+", "").Replace("BR", "").Trim();
                    bool containsPlus = bedRoom.Contains("+");

                    if (!containsPlus)
                    {
                        inventoryIDs.AddRange((from x in context.EndecaPropertyViews
                                               where x.Bedroom == int.Parse(br)
                                               select x.InventoryID).ToList());
                    }
                    else
                    {
                        inventoryIDs.AddRange((from x in context.EndecaPropertyViews
                                               where x.Bedroom >= int.Parse(br)
                                               select x.InventoryID).ToList());
                    }
                }

                return inventoryIDs;
            }
        }

        public EndecaPropertyView GetPropertyByInventoryID(long inventoryID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPropertyViews
                        where x.InventoryID == inventoryID
                        select x).FirstOrDefault();
            }
        }

        public class xtra { public long InventoryID; public bool isFeatured; };

        public Tuple<List<long>, List<long>> GetPropertyInventoryIDsByCountry(string status, string luxury, List<string> countries, string businessUnit)
        {
            if (status == String.Empty)
                status = "Active";

            bool isLuxury = luxury.Equals("1") ? true : false;

            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> returnInventoryIDs = new List<long>();
                List<long> returnFeaturedInventoryIDs = new List<long>();

                foreach (string country in countries)
                {
                    var props = (from pv in context.EndecaPropertyViews
                                 join g in context.EndecaGeographyViews
                                 on pv.InventoryID equals g.InventoryID
                                 join bu in context.EndecaBusinessUnits
                                 on pv.InventoryID equals bu.InventoryId
                                 where g.Country == country &&
                                 pv.Status == status &&
                                 pv.IsLuxury == isLuxury &&
                                 bu.BusinessUnitName == businessUnit
                                 select new xtra { InventoryID = pv.InventoryID, isFeatured = (bool)pv.IsFeaturedProperty});

                    foreach(xtra pv in props)
                    {
                        if (pv.isFeatured == true)
                            returnFeaturedInventoryIDs.Add(pv.InventoryID);

                        returnInventoryIDs.Add(pv.InventoryID);
                    }

                    //returnInventoryIDs.AddRange(var);
                }

                //return returnInventoryIDs;
                return new Tuple<List<long>, List<long>>(returnInventoryIDs, returnFeaturedInventoryIDs);
            }
        }

        public List<long> GetFeaturedPropertyIDsByInventoryIDs(List<long> inventoryIDs)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> featured = (from fv in context.EndecaPropertyViews
                                         where fv.IsFeaturedProperty == true //&&
                                         //inventoryIDs.Contains(fv.InventoryID)
                                         select fv.InventoryID).ToList();
                return featured;
                //return (from x in inventoryIDs
                //         join y in featured on
                //         x equals y
                //         select y).ToList();
            }
        }

        public List<long> GetPropertyInventoryIDsByDestinations(string status, string luxury, List<string> destinations, string businessUnit)
        {
            if (status == String.Empty)
                status = "Active";

            bool isLuxury = luxury.Equals("1") ? true : false;

            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> returnInventoryIDs = new List<long>();

                foreach (string dest in destinations)
                {
                    returnInventoryIDs.AddRange((from pv in context.EndecaPropertyViews
                                                 join g in context.EndecaGeographyViews
                                                 on pv.InventoryID equals g.InventoryID
                                                 join bu in context.EndecaBusinessUnits
                                                 on pv.InventoryID equals bu.InventoryId
                                                 where g.Destination == dest &&
                                                 pv.Status == status &&
                                                 pv.IsLuxury == isLuxury &&
                                                 bu.BusinessUnitName == businessUnit
                                                 select pv.InventoryID));
                }

                return returnInventoryIDs;
            }
        }

        public List<long> GetPropertyInventoryIDsByDestinationTypes(string status, string luxury, List<string> destinationTypes, string businessUnit)
        {
            if (status == String.Empty)
                status = "Active";

            bool isLuxury = luxury.Equals("1") ? true : false;

            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> returnInventoryIDs = new List<long>();

                foreach (string dest in destinationTypes)
                {
                    returnInventoryIDs.AddRange((from pv in context.EndecaPropertyViews
                                                 join g in context.EndecaGeographyViews
                                                 on pv.InventoryID equals g.InventoryID
                                                 join bu in context.EndecaBusinessUnits
                                                 on pv.InventoryID equals bu.InventoryId
                                                 where g.DestinationCategoryName == dest &&
                                                 pv.Status == status &&
                                                 pv.IsLuxury == isLuxury &&
                                                 bu.BusinessUnitName == businessUnit
                                                 select pv.InventoryID));
                }

                return returnInventoryIDs;
            }
        }
    }
}

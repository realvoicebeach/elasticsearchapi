﻿using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    class Connection
    {
        public string ConnectionString { get; set; }

        public ElasticSearchCoreDataContext GetContext()
        {
            ElasticSearchCoreDataContext dataContext = new ElasticSearchCoreDataContext(Properties.Settings.Default.LegoLandConnectionString);

            return dataContext;
        }
    }
}

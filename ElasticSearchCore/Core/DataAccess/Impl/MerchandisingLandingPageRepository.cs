﻿using System.Collections.Generic;
using System.Linq;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class MerchandisingLandingPageRepository : IMerchandisingLandingPageRepository
    {
        private Connection _connection;

        public MerchandisingLandingPageRepository()
        {
            _connection = new Connection();
        }

        public List<MerchandisingLandingPageView> GetAllLandingPages()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.MerchandisingLandingPageViews
                        select x).ToList();
            }
        }

        public List<MerchandisingLandingPageView> GetAllLandingPagesWithProperties()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPropertyViews
                        join y in context.MerchandisingLandingPageViews
                        on x.InventoryID equals y.InventoryID
                        select y).ToList();
            }
        }

        public List<MerchandisingLandingPageView> GetLandingPagesByInventoryID(long inventoryID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.MerchandisingLandingPageViews
                        where x.InventoryID == inventoryID
                        select x).ToList();
            }
        }
    }
}

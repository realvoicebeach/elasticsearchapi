﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class EndecaAmenitiesViewRepository : IEndecaAmenitiesViewRepository
    {
        private Connection _connection;

        public EndecaAmenitiesViewRepository()
        {
            _connection = new Connection();
        }

        public List<string> GetSplurgeAmenities()
        {
            List<string> splurge = new List<string>();

            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                //splurge.AddRange((from x in context.ComplexRoomTypeSplurgeAmenities
                // select x.SplurgeAmenity).Distinct());

                splurge.AddRange((from x in context.StandAloneRentalUnitRoomTypeSplurgeAmenities
                                  select x.SplurgeAmenity).Distinct());

                return splurge.Distinct().ToList();
            }
        }

        public List<EndecaAmenitiesView> GetAllAmenities()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaAmenitiesViews
                        select x).ToList();
            }
        }

        public List<EndecaAmenitiesView> GetAllAmenitiesWithProperties()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPropertyViews
                        join y in context.EndecaAmenitiesViews
                        on x.InventoryID equals y.InventoryID
                        select y).ToList();
            }
        }

        public List<EndecaAmenitiesView> GetAmenitiesByInventoryID(long inventoryID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaAmenitiesViews
                        where x.InventoryID == inventoryID
                        select x).ToList();
            }
        }
    }
}

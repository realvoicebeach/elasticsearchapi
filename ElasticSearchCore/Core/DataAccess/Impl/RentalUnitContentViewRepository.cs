﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class RentalUnitContentViewRepository : IRentalUnitContentViewRepository
    {
        private Connection _connection;

        public RentalUnitContentViewRepository()
        {
            _connection = new Connection();
        }

        public void AddRentalContentView(RentalUnitContentView ru)
        {
            throw new NotImplementedException();
        }

        public List<RentalUnitContentView> GetAllRentalContentView()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.RentalUnitContentViews
                        select x).ToList();
            }
        }

        public List<RentalUnitContentView> GetRentalContentViewByInventoryID(long inventoryId)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.RentalUnitContentViews
                        where x.InventoryId == inventoryId
                        select x).ToList();
            }
        }

        public List<long> GetInventoryIDsByCity(List<string> cities)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> inventoryIDs = new List<long>();

                foreach(string city in cities)
                {
                    inventoryIDs.AddRange((from x in context.RentalUnitContentViews
                     where x.City.ToLower() == city.ToLower()
                     select x.InventoryId).ToList());
                }

                return inventoryIDs;
            }
        }

        public List<RentalUnitContentView> GetAllRentalContentViewWithProperties()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPropertyViews
                        join y in context.RentalUnitContentViews
                        on x.InventoryID equals y.InventoryId
                        select y).ToList();
            }
        }
    }
}

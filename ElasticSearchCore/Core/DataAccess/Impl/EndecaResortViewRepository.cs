﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchCore.Core.Domain;

namespace ElasticSearchCore.Core.DataAccess.Impl
{
    public class EndecaResortViewRepository: IEndecaResortViewRepository
    {
        private Connection _connection;

        public EndecaResortViewRepository()
        {
            _connection = new Connection();
        }

        public List<EndecaResortView> GetAllResorts()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaResortViews
                        select x).ToList();
            }
        }

        public List<EndecaResortView> GetResortsByInventoryID(long inventoryID)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaResortViews
                        where x.InventoryID == inventoryID
                        select x).ToList();
            }
        }

        public List<long> GetInventoryIDsByResortName(List<string> resortNames)
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                List<long> inventoryIDs = new List<long>();

                foreach(string resortName in resortNames)
                {
                    inventoryIDs.AddRange((from x in context.EndecaResortViews
                                            where x.Resort.ToLower() == resortName
                                            select x.InventoryID).ToList());
                }

                return inventoryIDs;
            }
        }

        public List<EndecaResortView> GetAllResortsWithProperties()
        {
            using (ElasticSearchCoreDataContext context = _connection.GetContext())
            {
                return (from x in context.EndecaPropertyViews
                        join y in context.EndecaResortViews on
                        x.InventoryID equals y.InventoryID
                        select y).ToList();
            }
        }
    }
}

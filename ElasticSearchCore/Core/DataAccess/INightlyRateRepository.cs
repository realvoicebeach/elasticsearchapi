﻿using ElasticSearchCore.Core.Domain;
using ElasticSearchCore.Models.Insert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface INightlyRateRepository
    {
        List<NightlyAverage> GetYearAverageNightlyRate();
        List<NightlyRate> GetOneYearNightlyRatesForProperties();
    }
}

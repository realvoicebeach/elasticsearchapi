﻿using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface IRentalUnitContentViewRepository
    {
        void AddRentalContentView(RentalUnitContentView ru);
        List<RentalUnitContentView> GetAllRentalContentView();
        List<RentalUnitContentView> GetAllRentalContentViewWithProperties();
        List<RentalUnitContentView> GetRentalContentViewByInventoryID(long inventoryId);
        List<long> GetInventoryIDsByCity(List<string> cities);
    }
}

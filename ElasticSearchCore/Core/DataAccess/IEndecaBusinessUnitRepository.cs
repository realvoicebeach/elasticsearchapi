﻿using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface IEndecaBusinessUnitRepository
    {
        List<EndecaBusinessUnit> GetAllBusinessUnits();
        List<EndecaBusinessUnit> GetBusinessUnitsByBusinessUnitName(string businessUnitName);
        List<EndecaBusinessUnit> GetBusinessUnitsByInventoryID(long inventoryID);
        List<long> GetInventoryIDsByBusinessUnitNames(List<string> businessUnitNames);
        List<string> GetBusinessUnitNames();
    }
}

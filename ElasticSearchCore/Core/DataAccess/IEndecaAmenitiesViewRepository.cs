﻿using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface IEndecaAmenitiesViewRepository
    {
        List<EndecaAmenitiesView> GetAllAmenities();
        List<string> GetSplurgeAmenities();
        List<EndecaAmenitiesView> GetAllAmenitiesWithProperties();
        List<EndecaAmenitiesView> GetAmenitiesByInventoryID(long inventoryID);
    }
}

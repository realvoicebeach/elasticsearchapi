﻿using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface IEndecaWeeklyRatesViewRepository
    {
        List<EndecaWeeklyRatesView> GetAllWeeklyRates();
        List<EndecaWeeklyRatesView> GetAllWeeklyRatesWithProperties();
        List<EndecaWeeklyRatesView> GetWeeklyRatesByInventoryID(long inventoryID);
    }
}

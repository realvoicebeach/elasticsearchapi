﻿using ElasticSearchCore.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Core.DataAccess
{
    public interface IEndecaPromotionViewRepository
    {
        List<EndecaPromotionView> GetAllPromotions();
        List<EndecaPromotionView> GetAllPromotionsWithProperties();
        List<EndecaPromotionView> GetPromotionsByInventoryID(long inventoryID);
    }
}

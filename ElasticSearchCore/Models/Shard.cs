﻿
namespace ElasticSearch.Models
{
    public class Shard
    {
        public string total { get; set; }
        public string successful { get; set; }
        public string failed { get; set; }
    }
}

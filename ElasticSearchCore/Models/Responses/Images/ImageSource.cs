﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses.Images
{
    public class ImageSource
    {
        public ImageSource()
        {
            Images = new List<string>();
        }

        public long InventoryID { get; set; }
        public List<string> Images { get; set; }
    }
}

﻿using ElasticSearch.Models.Responses.Amenities;

namespace ElasticSearch.Models.Responses
{
    public class ElasticAmenitySearchResult
    {
        public string _scroll_id { get; set; }
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public AmenityHits hits { get; set; }
    }
}

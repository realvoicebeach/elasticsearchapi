﻿using ElasticSearch.Models;
using ElasticSearch.Models.Responses.PluginKeyDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses
{
    class ElasticPluginKeyDomainSearchResults
    {
        public string _scroll_id { get; set; }
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public PluginKeyDomainHits hits { get; set; }
    }
}

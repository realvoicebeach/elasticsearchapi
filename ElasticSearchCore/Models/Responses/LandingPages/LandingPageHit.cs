﻿
namespace ElasticSearch.Models.Responses.LandingPages
{
    public class LandingPageHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public LandingPageSource _source { get; set; }
    }
}

﻿
namespace ElasticSearch.Models.Responses.LandingPages
{
    public class LandingPageSource
    {
        public int InventoryID { get; set; }
        public string MerchandisingLandingPageName { get; set; }
    }
}

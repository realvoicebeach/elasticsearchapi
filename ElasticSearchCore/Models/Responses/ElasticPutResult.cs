﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses
{
    public class ElasticPutResult
    {
        public string acknowledged { get; set; }
        public string shards_acknowledged { get; set; }
    }
}

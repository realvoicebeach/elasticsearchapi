﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Models.Responses.WeeklyRates
{
    public class WeeklyRateHits
    {
        public WeeklyRateHit[] hits { get; set; }
    }
}

﻿
namespace ElasticSearch.Models.Responses.Amenities
{
    public class AmenitySource
    {
        public long InventoryID { get; set; }
        public string AttribName { get; set; }
    }
}

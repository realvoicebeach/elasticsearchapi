﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Models.Responses.Promotions
{
    public class PromotionSource
    {
        public int InventoryID { get; set; }
        public string AttribName { get; set; }
    }
}

﻿
namespace ElasticSearch.Models.Responses.Promotions
{
    public class PromotionHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public PromotionSource _source { get; set; }
    }
}

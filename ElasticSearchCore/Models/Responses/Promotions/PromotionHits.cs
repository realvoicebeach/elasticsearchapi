﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Models.Responses.Promotions
{
    public class PromotionHits
    {
        public PromotionHit[] hits { get; set; }
    }
}

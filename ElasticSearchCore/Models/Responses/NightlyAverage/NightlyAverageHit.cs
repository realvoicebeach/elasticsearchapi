﻿
namespace ElasticSearch.Models.Responses.NightlyAverage
{
    public class NightlyAverageHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public NightlyAverageSource _source { get; set; }
    }
}

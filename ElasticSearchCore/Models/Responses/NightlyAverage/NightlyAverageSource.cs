﻿
namespace ElasticSearch.Models.Responses.NightlyAverage
{
    public class NightlyAverageSource
    {
        public long InventoryID { get; set; }
        public decimal YearAverageRate { get; set; }
    }
}

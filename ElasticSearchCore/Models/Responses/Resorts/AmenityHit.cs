﻿
namespace ElasticSearch.Models.Responses.Resorts
{
    public class ResortHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public ResortSource _source { get; set; }
    }
}

﻿
namespace ElasticSearch.Models.Responses.Geography
{
    public class GeographyHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public GeographySource _source { get; set; }
    }
}

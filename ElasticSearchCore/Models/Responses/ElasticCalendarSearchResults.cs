﻿using ElasticSearch.Models;
using ElasticSearchCore.Models.Responses.Calendars;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses
{
    public class ElasticCalendarSearchResults
    {
        public string _scroll_id { get; set; }
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public CalendarHits hits { get; set; }
    }
}

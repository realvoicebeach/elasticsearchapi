﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses.BusinessUnits
{
    public class BusinessUnitSource
    {
        public long RentalUnitId { get; set; }
        public long InventoryId { get; set; }
        public long InventoryID { get; set; }
        public string BusinessUnitName { get; set; }
    }
}

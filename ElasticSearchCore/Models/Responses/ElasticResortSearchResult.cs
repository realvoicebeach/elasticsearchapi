﻿
using ElasticSearch.Models.Responses.Resorts;

namespace ElasticSearch.Models.Responses
{
    public class ElasticResortSearchResult
    {
        public string took { get; set; }
        public string timed_out { get; set; }
        public Shard _shards { get; set; }
        public ResortHits hits { get; set; }
    }
}

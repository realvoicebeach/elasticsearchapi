﻿
namespace ElasticSearch.Models.Responses.RentalUnitContent
{
    public class RentalUnitContentHits
    {
        public RentalUnitContentHit[] hits { get; set; }
    }
}

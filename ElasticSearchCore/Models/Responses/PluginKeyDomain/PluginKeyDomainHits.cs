﻿
namespace ElasticSearch.Models.Responses.PluginKeyDomain
{
    public class PluginKeyDomainHits
    {
        public PluginKeyDomainHit[] hits { get; set; }
    }
}

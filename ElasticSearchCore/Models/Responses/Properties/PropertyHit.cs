﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch.Models.Responses.Properties
{
    public class PropertyHit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public string _score { get; set; }
        public PropertySource _source { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double rate { get; set; }
        public int merchandiceOrder { get; set; }
    }
}

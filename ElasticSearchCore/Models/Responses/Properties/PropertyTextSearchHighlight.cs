﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchCore.Models.Responses.Properties
{
    public class PropertyTextSearchHighlight
    {
        public List<string> InventoryDescription { get; set; }
        public List<string> ComplexName { get; set; }
    }
}
